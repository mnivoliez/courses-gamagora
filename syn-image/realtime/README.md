# OpenGL - Realtime

The current implementation include mirror, portal and render to texture.

To run, just execute the *test* exec.
To compile, please use `make`.

Teste on Linux with opengl, glfw, glm.

## Not attended
- Shadow map

## What could be improved
The actual structure of the code do not please me. It seems rather difficult to correctly cut the code to obtain "reasonable" little bricks.
Could use a *Mesh* object.
