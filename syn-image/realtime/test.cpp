#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "glad/glad.h"

#include <GLFW/glfw3.h>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include "library.h"

// Store the global state of your program
struct
{
	GLuint program; // a shader
	GLuint vao; // a vertex array object
	size_t nTris;
        GLuint framebuffer;
        GLuint renderedTexture;

        GLuint floor_vao;
        size_t nTris_floor;
        GLuint portals_vaos[2];
        glm::vec3 portals_normal[2];
} gs;

const float globalScale = 0.5;
const float pi = glm::pi<float>();

const GLuint attribPosition = 0;

void init();
glm::mat4 mirror_view(glm::mat4 origin_view);
glm::mat4 portal_view(glm::mat4 origin_view, glm::mat4 portal_src, glm::mat4 portal_dest);
void render(const int width, const int height);

void init()
{
  // ========= FRAME BUFFFER ========== //
  GLuint framebuffer = 0;
  glGenFramebuffers(1, &framebuffer);
  gs.framebuffer = framebuffer;
  glBindFramebuffer(GL_FRAMEBUFFER, gs.framebuffer);
  glGenTextures(1, &gs.renderedTexture);
  glBindTexture(GL_TEXTURE_2D, gs.renderedTexture);
  glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGB8, 200, 200);
  std::vector<float> px(200 * 200 * 3, 0.5);

  glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 200, 200, GL_RGB, GL_FLOAT, px.data());
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, gs.renderedTexture, 0);
  if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
    std::cout << "incomplete";
  }

  // ========= MONKEY ========== //
  // load the stl mesh
  auto tris = readStl("monkey.stl");
  gs.nTris = tris.size();
  std::cout << tris.size() << std::endl;

  // Build our program and an empty VAO
  gs.program = buildProgram("basic.vsl", "basic.fsl");

  glGenVertexArrays(1, &gs.vao);
  glBindVertexArray(gs.vao);

  GLuint buffer;
  glGenBuffers(1, &buffer);
  glBindBuffer(GL_ARRAY_BUFFER, buffer);

  // fill the buffer
  int size = sizeof(Triangle) * tris.size();
  glBufferData(GL_ARRAY_BUFFER, size, nullptr, GL_STATIC_DRAW);
  glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Triangle) * tris.size(), tris.data());

  // set the VAO
  /*
    3: 3 floats
    GL_FLOAT: c'est des floats
    GL_FALSE: on ne veut pas les normaliser
    3 * 4 * 2: c'est l'espace entre chaque nombre
        3 float
            4 sizeof(float)
            2 (il y a les normals à passer)
   */
  glVertexAttribPointer(attribPosition, 3, GL_FLOAT, GL_FALSE, 3 * 4 * 2, 0);
  glEnableVertexAttribArray(attribPosition);

  // ========= Wall ========== // 
  // 2 tri by faces
  float vertices_floor[] = {
    -2.0f, -2.0f, -1.5f, //a
    2.0f, -2.0f, -1.5f, //b
    2.0f, 2.0f, -1.5f, //c

    -2.0f, -2.0f, -1.5f, //a
    -2.0f, 2.0f, -1.5f, //d
    2.0f, 2.0f, -1.5f, //c
  };
  glGenVertexArrays(1, &gs.floor_vao);
  glBindVertexArray(gs.floor_vao);

  GLuint floor_buffer;
  glGenBuffers(1, &floor_buffer);
  glBindBuffer(GL_ARRAY_BUFFER, floor_buffer);

  // fill the buffer
  int size_floor = sizeof(float) * 3 * 3 * 2;
  glBufferData(GL_ARRAY_BUFFER, size_floor, nullptr, GL_STATIC_DRAW);
  glBufferSubData(GL_ARRAY_BUFFER, 0, size_floor, vertices_floor);

  glVertexAttribPointer(attribPosition, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
  glEnableVertexAttribArray(attribPosition);

  // ========= Portals ========== // 
  float vertices_portal_1[] = {
    -2.0f, 3.0f, -2.0f, //a
    2.0f, 3.0f, -2.0f, //b
    2.0f, 3.0f, 2.0f, //c

    -2.0f, 3.0f, -2.0f, //a
    -2.0f, 3.0f, 2.0f, //d
    2.0f, 3.0f, 2.0f, //c
  };
  gs.portals_normal[0] = glm::vec3(0,4, 0);
  glGenVertexArrays(1, &gs.portals_vaos[0]);
  glBindVertexArray(gs.portals_vaos[0]);

  GLuint portal_1_buffer;
  glGenBuffers(1, &portal_1_buffer);
  glBindBuffer(GL_ARRAY_BUFFER, portal_1_buffer);

  // fill the buffer
  int size_portal_1 = sizeof(float) * 3 * 3 * 2;
  glBufferData(GL_ARRAY_BUFFER, size_portal_1, nullptr, GL_STATIC_DRAW);
  glBufferSubData(GL_ARRAY_BUFFER, 0, size_portal_1, vertices_portal_1);

  glVertexAttribPointer(attribPosition, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
  glEnableVertexAttribArray(attribPosition);


  float vertices_portal_2[] = {
    2.5f, 3.0f, -2.0f, //a
    6.5f, 3.0f, -2.0f, //b
    6.5f, 3.0f, 2.0f, //c

    2.5f, 3.0f, -2.0f, //a
    2.5f, 3.0f, 2.0f, //d
    6.5f, 3.0f, 2.0f, //c
  };
  gs.portals_normal[1] = glm::vec3(4,0, 0);
  glGenVertexArrays(1, &gs.portals_vaos[1]);
  glBindVertexArray(gs.portals_vaos[1]);

  GLuint portal_2_buffer;
  glGenBuffers(1, &portal_2_buffer);
  glBindBuffer(GL_ARRAY_BUFFER, portal_2_buffer);

  // fill the buffer
  int size_portal_2 = sizeof(float) * 3 * 3 * 2;
  glBufferData(GL_ARRAY_BUFFER, size_portal_2, nullptr, GL_STATIC_DRAW);
  glBufferSubData(GL_ARRAY_BUFFER, 0, size_portal_2, vertices_portal_2);

  glVertexAttribPointer(attribPosition, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
  glEnableVertexAttribArray(attribPosition);

  // ======= END ========== //
  glBindVertexArray(0);
}

glm::mat4 mirror_view(glm::mat4 origin_view){
  return glm::scale(
      glm::translate(origin_view, glm::vec3(0, 0, -3)),
      glm::vec3(1, 1, -1)
    );

}

glm::mat4 portal_view(glm::mat4 origin_view, glm::mat4 portal_src, glm::mat4 portal_dest) {
  auto mv = origin_view * portal_src;
  return mv 
    * glm::scale(glm::vec3(1, -1, 1))
    * glm::inverse(portal_dest);
}

void render(const int width, const int height)
{
  glEnable(GL_DEPTH_TEST);

  //make scene rotate
  //transform(width, height, gs.program);
  double time = glfwGetTime();

  float radius = 10;
  float fov = 80.0f* pi / 180.0f;
  float rotspeed = 0.5;
  auto cameraPos = glm::vec3( 
      radius * cos(10 + rotspeed*time),
      radius * sin(10 + rotspeed*time),
      0
  );

  glm::mat4 lookAt = glm::lookAt(cameraPos, glm::vec3(0, 0, 0), glm::vec3(0, 0, +1));
  //replace 4/3 by actual ratio
  glm::mat4 persp = glm::perspectiveFov(fov, (float)width, (float)height, 0.1f, 100.f);
  glm::mat4 object = glm::mat4();
  glm::mat4 transform = persp*lookAt*object;

  glProgramUniformMatrix4fv(gs.program, 4, 1, false, &transform[0][0]);

  // draw the monkey into the framebuffer
  glBindFramebuffer(GL_FRAMEBUFFER, gs.framebuffer);
  glViewport(0, 0 , 300, 300);
  glClearColor(1, 0.5, 1.0, 1);
  glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

  glBindVertexArray(gs.vao);
  glUseProgram(gs.program);
  glProgramUniform1f(gs.program, 1, 0);

  glDrawArrays(GL_TRIANGLES, 0, gs.nTris * 3);

  // draw the scene
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glViewport(0, 0 , width, height);
  glClearColor(0.7,0.7,0.7,0.7);
  glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

  glBindVertexArray(gs.vao);
  glProgramUniform1f(gs.program, 1, 1);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, gs.renderedTexture);

  glDrawArrays(GL_TRIANGLES, 0, gs.nTris * 3);

  glProgramUniform1f(gs.program, 1, 0);
  
  // draw mirror
  glEnable(GL_STENCIL_TEST);

    // Draw floor
    glStencilFunc(GL_ALWAYS, 1, 0xFF);
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
    glStencilMask(0xFF);
    glDepthMask(GL_FALSE);
    glClear(GL_STENCIL_BUFFER_BIT);
    glBindVertexArray(gs.floor_vao);
    glProgramUniform1f(gs.program, 2, 1);
    glDrawArrays(GL_TRIANGLES, 0, 6);

    glProgramUniform1f(gs.program, 2, 0);

    // Draw cube reflection
    glStencilFunc(GL_EQUAL, 1, 0xFF);
    glStencilMask(0x00);
    glDepthMask(GL_TRUE);

    glBindVertexArray(gs.vao);

    auto mirror_transform = mirror_view(transform);
    glProgramUniformMatrix4fv(gs.program, 4, 1, false, &mirror_transform[0][0]);

    glProgramUniform1f(gs.program, 1, 1);
    glDrawArrays(GL_TRIANGLES, 0, gs.nTris * 3);
    glProgramUniform1f(gs.program, 1, 0);

  glDisable(GL_STENCIL_TEST);

  glClearColor(0.2,0.2,0.2,0.2);

    
  glProgramUniformMatrix4fv(gs.program, 4, 1, false, &transform[0][0]);
  //portals logic

  glm::vec3 portalPos(0.0, 3.0, 0.0f);
  glm::vec3 portal2Pos(4.5, 3.0f, 0.0f);

  auto cam_to_portal_1 = portalPos - cameraPos;
  auto cam_to_portal_2 = portal2Pos - cameraPos;

  auto dot_portal1 = cam_to_portal_1.x * gs.portals_normal[0].x +cam_to_portal_1.y * gs.portals_normal[0].y + cam_to_portal_1.z * gs.portals_normal[0].z  ;
  auto dot_portal2 = cam_to_portal_2.x * gs.portals_normal[1].x +cam_to_portal_2.y * gs.portals_normal[1].y + cam_to_portal_2.z * gs.portals_normal[1].z  ;

  auto portal1Trans = glm::translate(portalPos);
  auto portal2Trans = glm::translate(portal2Pos);
  
  auto p1_transform = portal_view(transform, portal1Trans, portal2Trans);
  auto p2_transform = portal_view(transform, portal2Trans, portal1Trans);

  //draw portals 
  if(dot_portal1 > 0) {
    glEnable(GL_STENCIL_TEST);
      // portal stencil start
      glStencilFunc(GL_ALWAYS, 1, 0xFF); // Set any stencil to 1
      glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
      glStencilMask(0xFF); // Write to stencil buffer
      glDepthMask(GL_FALSE); // Don't write to depth 
      glClear(GL_STENCIL_BUFFER_BIT); // Clear stencil buffer (0 by default)

      glBindVertexArray(gs.portals_vaos[0]); 
      glProgramUniform1f(gs.program, 2, 1);
      glDrawArrays(GL_TRIANGLES, 0, 6);
      glProgramUniform1f(gs.program, 2, 0);

      glStencilFunc(GL_EQUAL, 1, 0xFF);
      glStencilMask(0x00);
      glDepthMask(GL_TRUE);

      glBindVertexArray(gs.vao);
  
      glProgramUniformMatrix4fv(gs.program, 4, 1, false, &p1_transform[0][0]);

    glProgramUniform1f(gs.program, 1, 1);
    glDrawArrays(GL_TRIANGLES, 0, gs.nTris * 3);
    glProgramUniform1f(gs.program, 1, 0);

    glDisable(GL_STENCIL_TEST);
  } else {
    glBindVertexArray(gs.portals_vaos[0]); 
    glProgramUniform1f(gs.program, 2, 1);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glProgramUniform1f(gs.program, 2, 0);  
  }

  glProgramUniformMatrix4fv(gs.program, 4, 1, false, &transform[0][0]);

  if(dot_portal2 > 0) {
    glEnable(GL_STENCIL_TEST);
      // portal stencil start
      glStencilFunc(GL_ALWAYS, 1, 0xFF); // Set any stencil to 1
      glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
      glStencilMask(0xFF); // Write to stencil buffer
      glDepthMask(GL_FALSE); // Don't write to depth 
      glClear(GL_STENCIL_BUFFER_BIT); // Clear stencil buffer (0 by default)

      glBindVertexArray(gs.portals_vaos[1]); 
      glProgramUniform1f(gs.program, 2, 1);
      glDrawArrays(GL_TRIANGLES, 0, 6);
      glProgramUniform1f(gs.program, 2, 0);

      glStencilFunc(GL_EQUAL, 1, 0xFF);
      glStencilMask(0x00);
      glDepthMask(GL_TRUE);

      glBindVertexArray(gs.vao);
  
      glProgramUniformMatrix4fv(gs.program, 4, 1, false, &p2_transform[0][0]);

      glProgramUniform1f(gs.program, 1, 1);
      glDrawArrays(GL_TRIANGLES, 0, gs.nTris * 3);
      glProgramUniform1f(gs.program, 1, 0);

    glDisable(GL_STENCIL_TEST);
  } else {
    glBindVertexArray(gs.portals_vaos[1]); 
    glProgramUniform1f(gs.program, 2, 1);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glProgramUniform1f(gs.program, 2, 0);
  }


  /*glProgramUniform1f(gs.program, 2, 0);
  glBindVertexArray(gs.portals_vaos[0]); 
  glDrawArrays(GL_TRIANGLES, 0, 6);

  glBindVertexArray(gs.portals_vaos[1]); 
  glDrawArrays(GL_TRIANGLES, 0, 6);*/

  glBindVertexArray(0);
  glUseProgram(0);
  glDisable(GL_DEPTH_TEST);
}

int main(void)
{
	runGL(init, render);
}
