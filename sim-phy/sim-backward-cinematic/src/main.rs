extern crate ggez;
use ggez::*;
use ggez::graphics::{DrawMode, Point2, Rect, Vector2};
use ggez::event::*;

struct Bone {
    length: f32,
    angle: f32,
}

struct MainState {
    start: Point2,
    bones: Vec<Bone>,
    points: Vec<Point2>,
    target: Point2,
}

impl MainState {
    fn new(_ctx: &mut Context) -> GameResult<MainState> {
        let s = MainState {
            start: Point2::new(400., 300.),
            bones: vec![
                Bone {
                    length: 50.,
                    angle: 30.,
                },
                Bone {
                    length: 50.,
                    angle: 30.,
                },
                Bone {
                    length: 50.,
                    angle: 30.,
                },
                Bone {
                    length: 50.,
                    angle: 30.,
                },
            ],
            points: vec![
                Point2::new(400., 300.),
                Point2::new(400., 300.),
                Point2::new(400., 300.),
                Point2::new(400., 300.),
                Point2::new(400., 300.),
            ],
            target: Point2::new(700., 300.),
        };
        Ok(s)
    }

    fn generate_points(&mut self) {
        self.points = align_points_to(&self.points, &self.bones, self.start, self.target);
    }
}

fn align_points_to(points: &[Point2], bones: &[Bone], from: Point2, to: Point2) -> Vec<Point2> {
    let mut f_pass = vec![to];

    for i in 0..bones.len() {
        let v = points[bones.len() - i] - f_pass[i];
        let vs = match v.try_normalize(1.) {
            Some(value) => value * bones[bones.len() - 1 - i].length,
            None => Vector2::new(1., 1.).normalize() * bones[bones.len() - 1 - i].length,
        };
        let p = f_pass[i] + vs;
        f_pass.push(p);
    }

    let mut s_pass = vec![from];

    f_pass.reverse();

    for i in 0..bones.len() {
        let v = f_pass[i + 1] - s_pass[i];
        let vs = match v.try_normalize(1.) {
            Some(value) => value * bones[i].length,
            None => Vector2::new(1., 1.) * bones[i].length,
        };
        let p = s_pass[i] + vs;
        if i > 0 {
            let vp = s_pass[i] - s_pass[i - 1];
            if !is_angle_correct(&vp, &vs, &bone[i]) {
                
            }
        }
        s_pass.push(p);
    }

    s_pass
}

//seems working
fn is_angle_correct(v0: &Vector2, v1: &Vector2, bone: &Bone) -> bool {
    v0.normalize().dot(&v1.normalize()) < bone.angle.cos()
}

impl event::EventHandler for MainState {
    fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        self.generate_points();
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx);
        graphics::line(ctx, &self.points, 2.)?;
        for p in &self.points {
            graphics::rectangle(
                ctx,
                DrawMode::Fill,
                Rect {
                    x: p.x,
                    y: p.y,
                    w: 2.,
                    h: 2.,
                },
            )?;
        }
        graphics::rectangle(
            ctx,
            DrawMode::Fill,
            Rect {
                x: self.start.x,
                y: self.start.y,
                w: 5.0,
                h: 5.0,
            },
        )?;
        graphics::rectangle(
            ctx,
            DrawMode::Fill,
            Rect {
                x: self.target.x,
                y: self.target.y,
                w: 5.,
                h: 5.,
            },
        )?;
        graphics::present(ctx);
        Ok(())
    }

    fn key_down_event(&mut self, ctx: &mut Context, keycode: Keycode, keymod: Mod, repeat: bool) {
        match keycode {
            Keycode::Space => self.generate_points(),
            Keycode::Escape => ctx.quit().unwrap(),
            _ => (),
        }
    }

    fn mouse_motion_event(
        &mut self,
        _ctx: &mut Context,
        _state: MouseState,
        x: i32,
        y: i32,
        _xrel: i32,
        _yrel: i32,
    ) {
        self.target = Point2::new(x as f32, y as f32);
    }
}

pub fn main() {
    let c = conf::Conf::new();
    let ctx = &mut Context::load_from_conf("super_simple", "ggez", c).unwrap();
    let state = &mut MainState::new(ctx).unwrap();
    event::run(ctx, state).unwrap();
}
