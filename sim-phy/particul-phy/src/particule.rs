use std::ops::{Add, AddAssign, Mul};
use ggez::graphics::{Point2, Vector2};

use rand::{thread_rng, Rng};

#[derive(Clone, Copy, Debug, PartialEq, PartialOrd)]
pub struct Velocity {
    pub value: Vector2,
}

#[derive(Clone, Copy, Debug, PartialEq, PartialOrd)]
pub struct Force {
    pub value: Vector2,
}

#[derive(Clone, Copy, Debug, PartialEq, PartialOrd)]
pub struct Particule {
    pub pred_pos: Point2,
    pub position: Point2,
    pub mass: f32,
    pub velocity: Velocity,
    pub density: f32,
}

impl Add for Force {
    type Output = Force;

    fn add(self, other: Force) -> Force {
        Force {
            value: self.value + other.value,
        }
    }
}

impl Mul<Force> for f32 {
    type Output = Force;
    fn mul(self, other: Force) -> Force {
        Force {
            value: self * other.value,
        }
    }
}

impl<'a> Mul<&'a Force> for f32 {
    type Output = Force;
    fn mul(self, other: &Force) -> Force {
        Force {
            value: self * other.value,
        }
    }
}
impl Add<Force> for Velocity {
    type Output = Velocity;

    fn add(self, other: Force) -> Velocity {
        Velocity {
            value: self.value + other.value,
        }
    }
}

impl Mul<f32> for Velocity {
    type Output = Velocity;
    fn mul(self, other: f32) -> Velocity {
        Velocity {
            value: self.value * other,
        }
    }
}

impl Add<Vector2> for Velocity {
    type Output = Velocity;
    fn add(self, other: Vector2) -> Velocity {
        Velocity {
            value: self.value + other,
        }
    }
}

impl AddAssign<Vector2> for Velocity {
    fn add_assign(&mut self, other: Vector2) {
        *self = *self + other;
    }
}

impl Add<Velocity> for Point2 {
    type Output = Point2;

    fn add(self, other: Velocity) -> Point2 {
        self + other.value
    }
}

impl Add<Force> for Point2 {
    type Output = Point2;

    fn add(self, other: Force) -> Point2 {
        self + other.value
    }
}

impl AddAssign<Force> for Point2 {
    fn add_assign(&mut self, other: Force) {
        *self = *self + other.value;
    }
}

pub fn generate_particules(nb: usize) -> Vec<Particule> {
    let mut particules = Vec::new();
    let mut rng = thread_rng();
    for _ in 0..nb {
        particules.push(Particule {
            pred_pos: Point2::new(0., 0.),
            position: Point2::new(
                rng.gen_range::<f32>(0., 800.),
                rng.gen_range::<f32>(0., 400.),
            ),
            mass: 0.1,
            velocity: Velocity {
                value: Vector2::new(0., 0.),
            },
            density: 0.,
        });
    }
    /*for i in 0..5 {
        for j in 0..5 {
            particules.push(Particule {
                pred_pos: Point2::new(0., 0.),
                position: Point2::new(200. + i as f32 * 20., 300. + j as f32 * 20.),
                mass: 10.,
                velocity: Velocity {
                    value: Vector2::new(0., 0.),
                },
                density: 0.,
            });
        }
    }*/
    particules
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn two_forces_should_output_one() {
        let (a1, a2) = (
            Force {
                value: Vector2::new(1., 1.),
            },
            Force {
                value: Vector2::new(2., 2.),
            },
        );

        assert_eq!(
            Force {
                value: Vector2::new(3., 3.),
            },
            a1 + a2
        );
    }

    #[test]
    fn one_force_should_change_velocity() {
        let (v, a) = (
            Velocity {
                value: Vector2::new(0., 0.),
            },
            Force {
                value: Vector2::new(0., 2.),
            },
        );

        assert_eq!(
            Velocity {
                value: Vector2::new(0., 2.),
            },
            v + a
        );
    }
}
