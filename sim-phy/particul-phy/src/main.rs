extern crate ggez;
use ggez::*;
use ggez::graphics::{DrawMode, Point2};

extern crate rand;

mod particule;
use particule::{generate_particules, Particule};

mod physic;

struct MainState {
    particules: Vec<Particule>,
}

impl MainState {
    fn new(_ctx: &mut Context) -> GameResult<MainState> {
        let s = MainState {
            particules: generate_particules(200),
        };
        Ok(s)
    }
}

impl event::EventHandler for MainState {
    fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        const DESIRED_FPS: u32 = 30;
        while timer::check_update_time(ctx, DESIRED_FPS) {
            let delta_time = 1.0 / (DESIRED_FPS as f32);
            physic::apply_physic(&mut self.particules, delta_time);
        }
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx);
        for particule in &self.particules {
            draw_particule(ctx, particule)?;
        }
        graphics::present(ctx);
        Ok(())
    }
}

fn draw_particule(ctx: &mut Context, particule: &Particule) -> GameResult<()> {
    let color = 1. - particule.density;
    graphics::set_color(ctx, graphics::Color::new(1., color, color, 1.0))?;
    graphics::circle(
        ctx,
        DrawMode::Fill,
        Point2::new(particule.position.x, particule.position.y),
        8.,
        2.0,
    )?;
    Ok(())
}

pub fn main() {
    let c = conf::Conf::new();
    let ctx = &mut Context::load_from_conf("super_simple", "ggez", c).unwrap();
    let state = &mut MainState::new(ctx).unwrap();
    event::run(ctx, state).unwrap();
}
