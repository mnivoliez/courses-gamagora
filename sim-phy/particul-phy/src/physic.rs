use ggez::graphics::{Point2, Vector2};
use particule::{Force, Particule, Velocity};

use std::collections::HashMap;
use ggez::nalgebra as na;

const GRAVITY: f32 = 9.81;
const GROUND_Y: f32 = 550.;
const LEFT_X: f32 = 0.;
const RIGHT_X: f32 = 800.;
const BORDER_REJECTION: f32 = 2.;
const CELL_SIZE: f32 = 30.;
const AIMED_DENSITY: f32 = 10_f32;
const K: f32 = 0.004;
const KNEAR: f32 = 0.01;

fn ground_bounce_force(pos: Point2) -> Option<Force> {
    let diff = pos.y - GROUND_Y;
    if diff > 0. {
        return Some(Force {
            value: Vector2::new(0., -GRAVITY * BORDER_REJECTION * diff),
        });
    }
    None
}

fn left_bounce_force(pos: Point2) -> Option<Force> {
    let diff = LEFT_X - pos.x;
    if diff > 0. {
        return Some(Force {
            value: Vector2::new(BORDER_REJECTION, 0.),
        });
    }
    None
}

fn right_bounce_force(pos: Point2) -> Option<Force> {
    let diff = pos.x - RIGHT_X;
    if diff > 0. {
        return Some(Force {
            value: Vector2::new(BORDER_REJECTION, 0.),
        });
    }
    None
}

pub fn apply_physic(particules: &mut Vec<Particule>, delta_time: f32) {
    //gravity
    for particule in particules.iter_mut() {
       particule.velocity += Vector2::new(0., particule.mass * GRAVITY) * delta_time;
    }

    for particule in particules.iter_mut() {
        particule.pred_pos = particule.position + particule.velocity * delta_time;
    }

    let delta_pow = delta_time.powi(2);

    for particule in particules.iter_mut() {
        if let Some(force) = ground_bounce_force(particule.position) {
            particule.pred_pos += delta_pow / particule.mass * force;
        }
        if let Some(force) = left_bounce_force(particule.position) {
            particule.pred_pos += delta_pow / particule.mass * force;
        }
        if let Some(force) = right_bounce_force(particule.position) {
            particule.pred_pos += delta_pow / particule.mass * force;
        }
    }

    let grid = map_particules_to_grid(particules);
    let particules_density = map_density_to_particules(particules, &grid);
    let particules_forces = map_forces_to_particule(particules, &particules_density);
    for (index, particule) in particules.iter_mut().enumerate() {
        if let Some(forces) = particules_forces.get(&index) {
            for force in forces {
                particule.pred_pos += delta_pow / particule.mass * force.value;
            }
        }
        particule.velocity = Velocity {
            value: (particule.pred_pos - particule.position) / delta_time,
        };
        particule.position = particule.pred_pos;
    }

    for (index, density, _) in particules_density {
        particules[index].density = density;
    }
}

fn map_forces_to_particule(
    particules: &[Particule],
    particules_density: &[(usize, f32, Vec<usize>)],
) -> HashMap<usize, Vec<Force>> {
    let mut particules_forces = HashMap::new();

    for &(p_index, density, ref applied_to) in particules_density {
        let i = particules[p_index].position;
        for p_affect in applied_to {
            let j = particules[*p_affect].position;
            let distance = na::distance(&i, &j);
            let mut ij: Vector2 = &j - &i;
            ij.normalize();
            let force = Force {
                value: (1_f32 - (distance / CELL_SIZE)) * (-density) * ij,
            };
            let forces = particules_forces.entry(*p_affect).or_insert(Vec::new());
            forces.push(force);
        }
    }

    particules_forces
}

fn map_density_to_particules(
    particules: &[Particule],
    grid: &HashMap<(i32, i32), Vec<usize>>,
) -> Vec<(usize, f32, Vec<usize>)> {
    let mut particules_density = Vec::new();
    for (index, particule) in particules.iter().enumerate() {
        let neigbourgs = particules_density_applied(particule, particules, grid);
        let density = K * (particule_density(particule, particules, &neigbourgs, 2) - AIMED_DENSITY)
            + KNEAR * (particule_density(particule, particules, &neigbourgs, 3));

        particules_density.push((index, density, neigbourgs));
    }
    particules_density
}

fn particules_density_applied(
    particule: &Particule,
    particules: &[Particule],
    grid: &HashMap<(i32, i32), Vec<usize>>,
) -> Vec<usize> {
    let (px, py) = (
        (particule.pred_pos.x / CELL_SIZE).floor() as i32,
        (particule.pred_pos.y / CELL_SIZE).floor() as i32,
    );
    let mut applied_to = Vec::new();
    for x in (px - 1)..(px + 1 + 1) {
        for y in (py - 1)..(py + 1 + 1) {
            if let Some(neigbourgs) = grid.get(&(x, y)) {
                for p in neigbourgs.iter() {
                    let distance = na::distance(&particule.pred_pos, &particules[*p].pred_pos);
                    if distance < CELL_SIZE {
                        applied_to.push(*p);
                    }
                }
            }
        }
    }
    applied_to
}

fn particule_density(
    particule: &Particule,
    particules: &[Particule],
    neigbourgs: &[usize],
    factor: i32,
) -> f32 {
    let mut density = 0_f32;
    for p in neigbourgs {
        let distance = na::distance(&particule.pred_pos, &particules[*p].pred_pos);
        if distance < CELL_SIZE {
            density += (1. - (distance / CELL_SIZE)).powi(factor) * &particules[*p].mass;
        }
    }
    density
}

fn map_particules_to_grid(particules: &[Particule]) -> HashMap<(i32, i32), Vec<usize>> {
    let mut particule_repartition: HashMap<(i32, i32), Vec<usize>> = HashMap::new();
    for (index, particule) in particules.iter().enumerate() {
        let (x, y) = (
            (particule.pred_pos.x / CELL_SIZE).floor() as i32,
            (particule.pred_pos.y / CELL_SIZE).floor() as i32,
        );
        let particules_group = particule_repartition.entry((x, y)).or_insert(Vec::new());
        particules_group.push(index);
    }
    particule_repartition
}
