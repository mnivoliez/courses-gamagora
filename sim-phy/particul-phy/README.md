# Particule physic simulation
I tried here to implement, to an extend, the research paper [Particle-based viscoelastic fluid simulation](https://www.researchgate.net/profile/Pierre_Poulin/publication/220789321_Particle-based_viscoelastic_fluid_simulation/links/0c96051824f22359e2000000.pdf).

## Gravity, boundaries and particules attraction

We applied first the gravity, then the boundaries forces and finaly, the "attraction forces".

## Density of particule
The attraction is calculate with some kind of mix of eq. 3 and eq. 6 from the paper.

As Rust forbid the to borrow in reading and in writting at the same time, the usage of map to link new value with their associate particule is kind of everywhere in the code. This lead to a separation of operations.

To speed up the calculation of density, we use a grid of spatial repartition.

## Spatial repatition
In order to make a spatial repartition efficiently, we use a spatial hash based on the coordinates of particule divided by a "CELL SIZE" wich allow to store neighbourgs close to each other and by that speed up the compute of density.

## Special thanks
* [Guillaume Bouchard](https://github.com/guibou) for advices and directives
* [msiglreith](https://github.com/msiglreith) for help in debug and advices
