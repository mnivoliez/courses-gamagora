extern crate ggez;
use ggez::*;
use ggez::graphics::{DrawMode, Point2};
use ggez::event::*;

#[derive(Debug)]
struct Skeleton {
    start: Point2,
    next_bone_index: usize,
    bones_length: Vec<f32>,
    bones_angle: Vec<f32>,
}

struct MainState {
    skeleton: Skeleton,
    points: Vec<Point2>,
}

impl MainState {
    fn new(_ctx: &mut Context) -> GameResult<MainState> {
        let s = MainState {
            skeleton: Skeleton {
                start: Point2::new(400., 300.),
                bones_length: vec![20., 30., 80., 48., 70., 80.],
                bones_angle: vec![30., 50., 40., -30., 85., 38.],
                next_bone_index: 4,
            },
            points: Vec::new(),
        };
        Ok(s)
    }

    fn generate_points(&mut self) {
        self.points.clear();
        self.points.push(self.skeleton.start);
        let mut last_point = self.skeleton.start;
        for i in 0..self.skeleton.next_bone_index {
            let cum_angle = self.skeleton.bones_angle[0..i + 1]
                .iter()
                .fold(0., |acc, n| acc + n);
            let x = self.skeleton.bones_length[i] * cum_angle.cos() + last_point.x;
            let y = self.skeleton.bones_length[i] * cum_angle.sin() + last_point.y;
            let p = Point2::new(x, y);
            self.points.push(p);
            last_point = p;
        }
    }
}

impl event::EventHandler for MainState {
    fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        const DESIRED_FPS: u32 = 60;
        while timer::check_update_time(ctx, DESIRED_FPS) {
            let seconds = 1.0 / (DESIRED_FPS as f32);
            self.skeleton.bones_angle[0] += 5. * seconds;
            self.skeleton.bones_angle[0] %= 360.;

            self.skeleton.bones_angle[1] += 1. * seconds;
            self.skeleton.bones_angle[1] %= 360.;


            self.skeleton.bones_angle[2] += 10. * seconds;
            self.skeleton.bones_angle[2] %= 360.;

            self.skeleton.bones_angle[3] += 8. * seconds;
            self.skeleton.bones_angle[3] %= 360.;
            
            self.skeleton.bones_angle[5] += 3. * seconds;
            self.skeleton.bones_angle[5] %= 360.;

        }
        self.generate_points();
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx);
        /*if let Some(target_p) = self.target {
            graphics::line(ctx, &[self.origin, target_p], 0.5)?;
        }*/
        graphics::line(ctx, &self.points, 1.)?;
        graphics::present(ctx);
        Ok(())
    }
}

pub fn main() {
    let c = conf::Conf::new();
    let ctx = &mut Context::load_from_conf("super_simple", "ggez", c).unwrap();
    let state = &mut MainState::new(ctx).unwrap();
    event::run(ctx, state).unwrap();
}
