extern crate ggez;
extern crate rand;

use ggez::*;
use ggez::graphics::{Color, DrawMode, Point2, Rect, Vector2};

use rand::Rng;

#[derive(Clone, Copy, Debug, PartialEq)]
enum PhysicType {
    Dynamic,
    Static,
}

#[derive(Clone, Copy, Debug)]
struct GameBox {
    physic_type: PhysicType,
    width: f32,
    height: f32,
    pos: Point2,
    mass: f32,
    speed: Vector2,
    color: Color,
}

#[derive(Clone, Debug)]
struct VirtualBox {
    pos: Point2,
    width: f32,
    height: f32,
}

struct MainState {
    boxes: Vec<GameBox>,
}

fn generate_random_box(n: usize) -> Vec<GameBox> {
    let mut rng = rand::thread_rng();
    (0..n)
        .map(|_| GameBox {
            physic_type: PhysicType::Dynamic,
            pos: Point2::new(rng.gen_range(0., 800.), rng.gen_range(0., 150.)),
            width: rng.gen_range(0., 100.),
            height: rng.gen_range(0., 100.),
            mass: rng.gen_range(0., 100.),
            speed: Vector2::new(0., 0.),
            color: Color::new(
                rng.gen_range(0., 1.),
                rng.gen_range(0., 1.),
                rng.gen_range(0., 1.),
                rng.gen_range(0.5, 1.),
            ),
        })
        .collect::<Vec<_>>()
}

impl MainState {
    fn new(_ctx: &mut Context) -> GameResult<MainState> {
        let mut boxes = generate_random_box(20);
        boxes.push(GameBox {
            physic_type: PhysicType::Static,
            pos: Point2::new(0., 500.),
            width: 800.,
            height: 50.,
            mass: 10.,
            speed: Vector2::new(0., 0.),
            color: Color::new(0.8, 0.8, 0.8, 1.0),
        });
        let s = MainState { boxes };
        Ok(s)
    }
}

impl event::EventHandler for MainState {
    fn update(&mut self, ctx: &mut Context) -> GameResult<()> {
        const DESIRED_FPS: u32 = 60;
        let boxes_collide = detect_collision(&self.boxes);

        while timer::check_update_time(ctx, DESIRED_FPS) {
            let delta_time = 1.0 / (DESIRED_FPS as f32);
            for (game_box, collide_another) in self.boxes.iter_mut().zip(&boxes_collide) {
                match game_box.physic_type {
                    PhysicType::Dynamic => if !collide_another {
                        apply_gravity_on_box(game_box, delta_time);
                    },
                    PhysicType::Static => (),
                }
            }
        }
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx);
        for game_box in &self.boxes {
            draw_box(ctx, game_box)?;
        }
        graphics::present(ctx);
        Ok(())
    }
}

fn detect_collision(boxes: &[GameBox]) -> Vec<bool> {
    boxes
        .iter()
        .map(|game_box| {
            boxes.iter().any(|other| {
                does_box_collide_box(game_box, other) && is_box_below_box(&other, game_box)
            })
        })
        .collect::<Vec<_>>()
}

fn does_box_collide_box(game_box: &GameBox, other_box: &GameBox) -> bool {
    if game_box.pos == other_box.pos {
        return false;
    }
    let (gb_half_width, gb_half_height) = (game_box.width / 2., game_box.height / 2.);
    let (ob_half_width, ob_half_height) = (other_box.width / 2., other_box.height / 2.);
    let (gb_x_min, gb_x_max, gb_y_min, gb_y_max) = (
        game_box.pos.x - gb_half_width,
        game_box.pos.x + gb_half_width,
        game_box.pos.y - gb_half_height,
        game_box.pos.y + gb_half_height,
    );
    let (ob_x_min, ob_x_max, ob_y_min, ob_y_max) = (
        other_box.pos.x - ob_half_width,
        other_box.pos.x + ob_half_width,
        other_box.pos.y - ob_half_height,
        other_box.pos.y + ob_half_height,
    );
    let inside_x = gb_x_min < ob_x_max && gb_x_max > ob_x_min;
    let inside_y = gb_y_min < ob_y_max && gb_y_max > ob_y_min;
    inside_x && inside_y
}

fn is_box_below_box(first_box: &GameBox, second_box: &GameBox) -> bool {
    first_box.pos.y > second_box.pos.y
}

fn apply_gravity_on_box(game_box: &mut GameBox, delta_time: f32) {
    game_box.speed += delta_time * compute_gravitational_acceleration(game_box.mass);
    game_box.pos += game_box.speed;
}

fn compute_gravitational_acceleration(mass: f32) -> Vector2 {
    return Vector2::new(0., 9.8 / mass);
}

fn draw_box(ctx: &mut Context, game_box: &GameBox) -> GameResult<()> {
    graphics::set_color(ctx, game_box.color)?;
    graphics::rectangle(
        ctx,
        DrawMode::Fill,
        Rect::new(
            game_box.pos.x,
            game_box.pos.y,
            game_box.width,
            game_box.height,
        ),
    )?;

    Ok(())
}

pub fn main() {
    let c = conf::Conf::new();
    let ctx = &mut Context::load_from_conf("super_simple", "ggez", c).unwrap();
    let state = &mut MainState::new(ctx).unwrap();
    event::run(ctx, state).unwrap();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_detect_no_collision_between_box() {
        let b1 = GameBox {
            physic_type: PhysicType::Dynamic,
            pos: Point2::new(0., 0.),
            width: 50.,
            height: 50.,
            mass: 10.,
            speed: Vector2::new(0., 0.),
            color: Color::new(0.8, 0.8, 0.8, 1.0),
        };
        let b2 = GameBox {
            physic_type: PhysicType::Dynamic,
            pos: Point2::new(0., 500.),
            width: 50.,
            height: 50.,
            mass: 10.,
            speed: Vector2::new(0., 0.),
            color: Color::new(0.8, 0.8, 0.8, 1.0),
        };

        assert!(!does_box_collide_box(&b1, &b2));
    }

    #[test]
    fn should_not_collide_with_itself() {
        let b1 = GameBox {
            physic_type: PhysicType::Dynamic,
            pos: Point2::new(0., 0.),
            width: 50.,
            height: 50.,
            mass: 10.,
            speed: Vector2::new(0., 0.),
            color: Color::new(0.8, 0.8, 0.8, 1.0),
        };
        assert!(!does_box_collide_box(&b1, &b1));
    }

    #[test]
    fn should_detect_collision_between_box() {
        let b1 = GameBox {
            physic_type: PhysicType::Dynamic,
            pos: Point2::new(0., 0.),
            width: 50.,
            height: 50.,
            mass: 10.,
            speed: Vector2::new(0., 0.),
            color: Color::new(0.8, 0.8, 0.8, 1.0),
        };
        let b2 = GameBox {
            physic_type: PhysicType::Dynamic,
            pos: Point2::new(25., 25.),
            width: 50.,
            height: 50.,
            mass: 10.,
            speed: Vector2::new(0., 0.),
            color: Color::new(0.8, 0.8, 0.8, 1.0),
        };

        assert!(does_box_collide_box(&b1, &b2));
    }
}
