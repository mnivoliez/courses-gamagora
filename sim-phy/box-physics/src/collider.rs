#[derive(Clone, Debug)]
struct ColliderBox {
    pos: Point2,
    width: f32,
    height: f32,
}

#[derive(Debug)]
pub struct Collision<T: Collidable> {
   first_collider: T,
   second_collider: T,
}

pub Trait Collidable {
    fn does_collide<C: Collidable>(collider: &T)-> Collision;
}
fn detect_collision(boxes: &[GameBox]) {
    boxes
        .iter()
        .map(|game_box| {
            self.boxes.iter().any(|other| {
                does_box_collide_box(game_box, other) && is_box_below_box(&other, game_box)
            })
        })
        .collect::<Vec<_>>();
}
