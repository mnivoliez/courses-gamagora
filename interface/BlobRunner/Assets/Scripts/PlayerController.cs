﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    private ColorDetection _colorDetection;
    [SerializeField] float _speed;
    [SerializeField] float _jumpForce;
    [SerializeField] bool isGrounded = true;
    [SerializeField] float _jumpTimeout;
    private float _timeJumping;

	// Use this for initialization
	void Start () {
        _colorDetection = GameObject.FindGameObjectWithTag("GameController").GetComponent<ColorDetection>();
	}
	
	// Update is called once per frame
	void Update () {
	}

    private void FixedUpdate()
    {
        Vector2 centroid = _colorDetection.AquireCentroid();
        isGrounded = Physics.Raycast(gameObject.transform.position, Vector3.down, gameObject.transform.localScale.y);
        Vector3 movedir;
        if(centroid.x > 0)
        {
            movedir = new Vector3(_speed * Time.fixedDeltaTime, 0,0);
        } else
        {
            movedir = new Vector3(-_speed * Time.fixedDeltaTime, 0, 0);
        }
        bool jump_requested = centroid.y > 0;
        if(isGrounded && jump_requested)
        {
            _timeJumping = 0;
            movedir += new Vector3(0, _jumpForce * Time.deltaTime, 0);
        } else if(jump_requested && _timeJumping < _jumpTimeout)
        {
            _timeJumping += Time.fixedDeltaTime;
            movedir += new Vector3(0, _jumpForce * Time.deltaTime, 0);
        }

        transform.Translate(movedir, Space.World);

        // Get sure it stays on at 0 in z
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
    }

}
