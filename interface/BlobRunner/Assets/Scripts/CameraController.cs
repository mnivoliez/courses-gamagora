﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    private PlayerController _player;
    private bool _isSet = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(_isSet)
        {
            var pos = transform.position;
            pos.x = _player.transform.position.x;
            transform.position = pos;
        }
	}

    public void FollowPlayer(PlayerController player)
    {
        _player = player;
        _isSet = true;
    }

    public void PlayerDied()
    {
        _isSet = false;
    }
}
