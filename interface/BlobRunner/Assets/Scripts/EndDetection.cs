﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndDetection : MonoBehaviour {
    [SerializeField] private GameManager _manager;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Enter here");
        if(other.CompareTag("Player"))
        {
            _manager.Win();
            Destroy(other.gameObject);
        }
    }
}
