﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    [SerializeField] private int _life;
    [SerializeField] private GameObject _spawn;
    [SerializeField] private PlayerController _playerPrefab;
    [SerializeField] private CameraController _camera;
    [SerializeField] private Text _lifeText;
    [SerializeField] private Text _endText;



	// Use this for initialization
	void Start () {
        SpawnPlayer();
        _lifeText.text = "Life: " + _life;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void SpawnPlayer()
    {
        PlayerController instance = Instantiate(_playerPrefab, _spawn.transform.position + Vector3.up * 2, new Quaternion());
        _camera.FollowPlayer(instance);
    }

    public void PlayerDied()
    {
        _camera.PlayerDied();
        PlayerController instance = Instantiate(_playerPrefab, _spawn.transform.position + Vector3.up * 2, new Quaternion());
        _camera.FollowPlayer(instance);
        _life--;
        if (_life <= 0) Loose();
        _lifeText.text = "Life: " + _life;
    }

    public void Win()
    {
        Time.timeScale = 0f;
        _endText.text = "You winned!";
    }

    private void Loose()
    {
        Time.timeScale = 0f;
        _endText.text = "You loose!";
    }
}
