﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Emgu.CV;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

using System;
using System.Drawing;
using System.IO;
using UnityEngine.UI;

enum Filter
{
    blur,
    median_blur,
    gaussian_blur
}

public class ColorDetection : MonoBehaviour {

    [Header("HSV low")]
    [SerializeField] [Range(0, 179)] float _h_low_value;
    [SerializeField] [Range(0, 255)] float _s_low_value;
    [SerializeField] [Range(0, 255)] float _v_low_value;

    [Header("HSV hight")]
    [SerializeField][Range(0, 179)] float _h_hight_value;
    [SerializeField][Range(0, 255)] float _s_hight_value;
    [SerializeField][Range(0, 255)] float _v_hight_value;

    [Header("Filter")]
    [SerializeField] Filter _filter;
    [SerializeField] int _filter_size;
    [SerializeField] int _operation_size;
    [SerializeField] int _erode_iter;
    [SerializeField] int _dilate_iter;

    [Header("Contours")]
    [SerializeField][Range(0, 255)] float _contour_r;
    [SerializeField][Range(0, 255)] float _contour_b;
    [SerializeField][Range(0, 255)] float _contour_g;
    [SerializeField] [Range(0, 10)] int _contour_size;


    [Header("Webcam")]
    [SerializeField] int _webcamId;
    [SerializeField] RawImage _uiFrame;
    private VideoCapture _webcam;

    private Vector2 _centroid;

    private Mat _webcamFrame;
    private Mat _finalFrame;
    private Texture2D _output;

    // Use this for initialization
    void Start () {
        //on init la webcam
        _webcam = new VideoCapture(_webcamId);
        // on init les Matrice contenant les images
        _webcamFrame = new Mat();
        _finalFrame = new Mat();
        // on defini un evant handler pour le grab de la webcam
        _webcam.ImageGrabbed += new EventHandler(webcamQueryFrameHandler);
        _output = new Texture2D(150, 100, TextureFormat.RGBA32, false);
    }

    private void webcamQueryFrameHandler(object sender, EventArgs e)
    {
        // si la webcam est ouverte, grab dans la matrice associé
        if (_webcam.IsOpened) _webcam.Retrieve(_webcamFrame);
        // si la frame est vide, on ne continue pas
        if (_webcamFrame.IsEmpty) return;
        // on flip l'image pour en fair eun mirroir du user
        CvInvoke.Flip(_webcamFrame, _webcamFrame, FlipType.Horizontal);

        // on definit une matrice temporaire
        Mat frameHSV = new Mat();
        // on converti la matrice _webcamFrame en HSV
        CvInvoke.CvtColor(_webcamFrame, frameHSV, ColorConversion.Bgr2Hsv);

        frameHSV = filterImage(frameHSV, _filter, _filter_size);

        // on utilise le wrapper Image de EMGUcv
        Image<Hsv, byte> imageHSV = frameHSV.ToImage<Hsv, byte>();

        // on defini les filtre HSV
        Hsv lower = new Hsv(_h_low_value, _s_low_value, _v_low_value);
        Hsv upper = new Hsv(_h_hight_value, _s_hight_value, _v_hight_value);

        // On met l'image en niveau de gris
        Mat imgGray = imageHSV.InRange(lower, upper).Mat;

        // on défini un élément structurant
        Mat structuringElement = CvInvoke.GetStructuringElement(ElementShape.Rectangle, 
            new Size(2 * _operation_size + 1, 2 * _operation_size + 1), 
            new Point(_operation_size, _operation_size));

        // on erode puis dilate pour supprimer les petites zones a part.
        CvInvoke.Erode(imgGray,
            imgGray,
            structuringElement,
            new Point(-1, -1),
            _erode_iter,
            BorderType.Constant,
            new MCvScalar(0));
        CvInvoke.Dilate(imgGray,
            imgGray,
            structuringElement,
            new Point(-1, -1),
            _dilate_iter,
            BorderType.Constant,
            new MCvScalar(0));

        // on definit les structures pour contenir les contours.
        VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
        VectorOfPoint biggestContour = new VectorOfPoint() ;
        int biggestContourIndex = -1;
        double biggestContourArea = 0;

        Mat hierarchy = new Mat();
        // on cherche tous les contours
        CvInvoke.FindContours(imgGray, contours, hierarchy, RetrType.List, ChainApproxMethod.ChainApproxNone);

        // on trouve le plus gros contours
        for(int i = 0; i < contours.Size; i++)
        {
            if(CvInvoke.ContourArea(contours[i]) > biggestContourArea)
            {
                biggestContour = contours[i];
                biggestContourIndex = i;
                biggestContourArea = CvInvoke.ContourArea(contours[i]);
            }
        }
        if (biggestContourIndex > -1)
        {
            // on dessine le contours sur la frame
            CvInvoke.DrawContours(_webcamFrame, contours, biggestContourIndex, new MCvScalar(_contour_b, _contour_g, _contour_r), _contour_size);

            // on trouve le centroid
            var moments = CvInvoke.Moments(biggestContour);
            double cx = moments.M10 / moments.M00;
            double cy = moments.M01 / moments.M00;
            _centroid = new Vector2((float)cx / _webcamFrame.Width, (float)cy / _webcamFrame.Height);
            _centroid -= new Vector2(0.5f, 0.5f);
            _centroid.y *= -1;
            Point centroid = new Point((int)cx, (int)cy);

            // on dessine le centroid
            CvInvoke.Circle(_webcamFrame, centroid, 5, new MCvScalar(_contour_b, _contour_g, _contour_r));
        } else
        {
            _centroid = new Vector2(-1, -1);
        }
    }
    
    private Mat filterImage(Mat image, Filter filter, int size)
    {
        // on filtre l'image selong blu, median blur ou gauss
        Mat output = new Mat();
        switch (filter)
        {
            case Filter.blur:
                CvInvoke.Blur(image, output, new Size(size, size), new Point(-1, -1));
                break;
            case Filter.median_blur:
                CvInvoke.MedianBlur(image, output, size);
                break;
            case Filter.gaussian_blur:
                CvInvoke.GaussianBlur(image, output, new Size(_filter_size, _filter_size), _filter_size / 2.0);
                break;
        }
        return output;
    }

    // Update is called once per frame
    void Update () {
        if (_webcam.IsOpened) _webcam.Grab();
        //CvInvoke.Imshow("webcam", _webcamFrame);
        // on envois l'image sur l'ui
        CvInvoke.Resize(_webcamFrame, _finalFrame, new Size(150, 100));
        CvInvoke.Flip(_finalFrame, _finalFrame, FlipType.Vertical);
        _output.LoadRawTextureData(_finalFrame.ToImage<Rgba, byte>().Bytes);
        _output.Apply();
        _uiFrame.texture = _output;
    }

    public Vector2 AquireCentroid()
    {
        return _centroid;
    }
    private void OnDestroy()
    {
        // sur la destuction de l'object, on ferme toutes les mécanique de capture.
        _webcam.Dispose();
        CvInvoke.DestroyAllWindows();
    }
}
