﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Emgu.CV;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

using System;
using System.Drawing;
using System.IO;

enum Filter
{
    blur,
    median_blur,
    gaussian_blur
}

public class ColorDetection : MonoBehaviour {

    [Header("HSV low")]
    [SerializeField] [Range(0, 179)] float _h_low_value;
    [SerializeField] [Range(0, 255)] float _s_low_value;
    [SerializeField] [Range(0, 255)] float _v_low_value;

    [Header("HSV hight")]
    [SerializeField][Range(0, 179)] float _h_hight_value;
    [SerializeField][Range(0, 255)] float _s_hight_value;
    [SerializeField][Range(0, 255)] float _v_hight_value;

    [Header("Filter")]
    [SerializeField] Filter _filter;
    [SerializeField] int _filter_size;
    [SerializeField] int _operation_size;
    [SerializeField] int _erode_iter;
    [SerializeField] int _dilate_iter;

    [Header("Contours")]
    [SerializeField][Range(0, 255)] float _contour_r;
    [SerializeField][Range(0, 255)] float _contour_b;
    [SerializeField][Range(0, 255)] float _contour_g;
    [SerializeField] [Range(0, 10)] int _contour_size;


    [Header("Webcam")]
    [SerializeField] int _webcamId;
    private VideoCapture _webcam;

    private Mat _webcamFrame;

    // Use this for initialization
    void Start () {
        _webcam = new VideoCapture(_webcamId);
        _webcamFrame = new Mat();
        _webcam.ImageGrabbed += new EventHandler(webcamQueryFrameHandler);
    }

    private void webcamQueryFrameHandler(object sender, EventArgs e)
    {
        if (_webcam.IsOpened) _webcam.Retrieve(_webcamFrame);
        if (_webcamFrame.IsEmpty) return;
        CvInvoke.Flip(_webcamFrame, _webcamFrame, FlipType.Horizontal);

        Mat frameHSV = new Mat();
        CvInvoke.CvtColor(_webcamFrame, frameHSV, ColorConversion.Bgr2Hsv);

        frameHSV = filterImage(frameHSV, _filter, _filter_size);

        Image<Hsv, byte> imageHSV = frameHSV.ToImage<Hsv, byte>();

        Hsv lower = new Hsv(_h_low_value, _s_low_value, _v_low_value);
        Hsv upper = new Hsv(_h_hight_value, _s_hight_value, _v_hight_value);

        Mat imgGray = imageHSV.InRange(lower, upper).Mat;

        Mat structuringElement = CvInvoke.GetStructuringElement(ElementShape.Rectangle, 
            new Size(2 * _operation_size + 1, 2 * _operation_size + 1), 
            new Point(_operation_size, _operation_size));

        CvInvoke.Erode(imgGray,
            imgGray,
            structuringElement,
            new Point(-1, -1),
            _erode_iter,
            BorderType.Constant,
            new MCvScalar(0));
        CvInvoke.Dilate(imgGray,
            imgGray,
            structuringElement,
            new Point(-1, -1),
            _dilate_iter,
            BorderType.Constant,
            new MCvScalar(0));

        VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
        VectorOfPoint biggestContour;
        int biggestContourIndex = -1;
        double biggestContourArea = 0;

        Mat hierarchy = new Mat();
        CvInvoke.FindContours(imgGray, contours, hierarchy, RetrType.List, ChainApproxMethod.ChainApproxNone);

        for(int i = 0; i < contours.Size; i++)
        {
            if(CvInvoke.ContourArea(contours[i]) > biggestContourArea)
            {
                biggestContour = contours[i];
                biggestContourIndex = i;
                biggestContourArea = CvInvoke.ContourArea(contours[i]);
            }
        }
        if (biggestContourIndex > -1) CvInvoke.DrawContours(_webcamFrame, contours, biggestContourIndex, new MCvScalar(_contour_b, _contour_g, _contour_r), _contour_size);
    }
    
    private Mat filterImage(Mat image, Filter filter, int size)
    {
        Mat output = new Mat();
        switch (filter)
        {
            case Filter.blur:
                CvInvoke.Blur(image, output, new Size(size, size), new Point(-1, -1));
                break;
            case Filter.median_blur:
                CvInvoke.MedianBlur(image, output, size);
                break;
            case Filter.gaussian_blur:
                CvInvoke.GaussianBlur(image, output, new Size(_filter_size, _filter_size), _filter_size / 2.0);
                break;
        }
        return output;
    }

    // Update is called once per frame
    void Update () {
        if (_webcam.IsOpened) _webcam.Grab();
        CvInvoke.Imshow("webcam", _webcamFrame);
    }

    private void OnDestroy()
    {
        _webcam.Dispose();
        CvInvoke.DestroyAllWindows();
    }
}
