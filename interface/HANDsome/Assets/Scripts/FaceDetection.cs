﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Emgu.CV;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

using System;
using System.Drawing;
using System.IO;

public class FaceDetection : MonoBehaviour {

    private VideoCapture _webcam;
    [SerializeField] int _webcamId;
    private Mat _webcamFrame;
    private Mat _webcamFrameGray;
    private CascadeClassifier _faceCascadeClassifier;
    private MCvScalar _localScalar;
    private string _classifierFileName = "/Haarcascade/haarcascade_frontalcatface.xml";

    private Rectangle[] _faces;
    [SerializeField] int _minFaceSize = 30;
    [SerializeField] int _maxFaceSize = 200;

    // Use this for initialization
    void Start () {
        _webcam = new VideoCapture(_webcamId);
        _webcamFrame = new Mat();
        _webcamFrameGray = new Mat();
        _webcam.ImageGrabbed += new EventHandler(_faceleWebcamQueryFrame);
        _faceCascadeClassifier = new CascadeClassifier(fileName: Application.dataPath +_classifierFileName);
        _localScalar = new MCvScalar(0, 180, 0); 
	}
	
	// Update is called once per frame
	void Update () {
        if (_webcam.IsOpened) _webcam.Grab();
        if (_webcamFrameGray.IsEmpty) return;
        _faces = _faceCascadeClassifier.DetectMultiScale(
            image: _webcamFrameGray, 
            scaleFactor: 1.1, 
            minNeighbors: 5, 
            minSize: new Size(_minFaceSize, _minFaceSize), 
            maxSize: new Size(_maxFaceSize, _maxFaceSize));
        foreach(Rectangle face in _faces)
        {
            CvInvoke.Rectangle(_webcamFrame, face, _localScalar, 5);
        }
        CvInvoke.Imshow("test face", _webcamFrame);

    }

    private void _faceleWebcamQueryFrame(object sender, EventArgs e)
    {
        if (_webcam.IsOpened) _webcam.Retrieve(_webcamFrame);
        if (_webcamFrame.IsEmpty) return;
        CvInvoke.Flip(_webcamFrame, _webcamFrame, FlipType.Horizontal);

        CvInvoke.CvtColor(_webcamFrame, _webcamFrameGray, ColorConversion.Bgr2Gray);
        if (_webcamFrameGray.IsEmpty) return;
    }

    private void OnDestroy()
    {
        _webcam.Dispose();
        CvInvoke.DestroyAllWindows();
    }
}
