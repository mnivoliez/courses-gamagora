﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineSight : MonoBehaviour {
	public enum SightSensitivity { STRICT, LOOSE };
	public SightSensitivity sensitivity = SightSensitivity.STRICT;
	private Transform thisTransform = null;
	private SphereCollider thisCollider = null;
	public Vector3 LastKnownSighting = Vector3.zero;
	
	public float fieldOfView = 45f;
	public Transform target = null;
	public Transform eyePoint =	null;
	public bool canSeeTarget = false;

	void Awake(){
		thisTransform = GetComponent<Transform> ();
		thisCollider = GetComponent<SphereCollider>();
		LastKnownSighting = thisTransform.position;
	}

	bool InFOV () {
		Vector3 DirToTarget = target.position - eyePoint.position;
		float angle = Vector3.Angle ( eyePoint.forward, DirToTarget );
		if (angle <= fieldOfView) {
			return true;
		}
		return false;
	}

	bool ClearLineOfSight () {
		RaycastHit info;
		if(Physics.Raycast(eyePoint.position, (target.position - eyePoint.position).normalized,	out info, thisCollider.radius)) {
			if (info.transform.CompareTag("Player")) {
				return true;
			}
		}
		return false;
	}

	void UpdateSight () {
		switch( sensitivity	) {
		case SightSensitivity.STRICT:
			canSeeTarget = InFOV() && ClearLineOfSight();
			break;
		case SightSensitivity.LOOSE:
			canSeeTarget = InFOV () || ClearLineOfSight ();
			break;
		}
	}

	void OnTriggerStay (Collider other) {
		UpdateSight ();
		if(canSeeTarget) {
			LastKnownSighting = target.position;
		}
	}
}
