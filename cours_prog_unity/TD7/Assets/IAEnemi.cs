﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IAEnemi : MonoBehaviour {
	private NavMeshAgent thisAgent = null;
	public Transform PatrolDestination = null;

	void Awake() {
		thisAgent = GetComponent<NavMeshAgent> ();
	}

	void Update () {
		thisAgent.SetDestination(PatrolDestination.position);
	}
}
