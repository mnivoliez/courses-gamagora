﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereBehavior : MonoBehaviour {

	[SerializeField] GameObject togglable;
	[SerializeField] private float rotation_speed;
	[SerializeField] private float rotate_angle;

	/** 
	 * Awake is use at the game initialisation only, regardless if the script is enable of not and before Start. 
	 * It should not be used to pass information like variable initialization to script. 
	 **/
	void Awake() {
		Debug.Log ("Awake is called!");
	}

	/**
	 * Start is used at the initialization of the script only if it is enabled. 
	 **/
	void Start () {
		Debug.Log ("Start is called!");
		/**
		 * Show time spend since last frame; 
		 **/
		Debug.Log (Time.deltaTime);
	}
	
	/**
	 * Update is called at each frame and thus depends of rendering loop.
	 * In case of slow render, the regularity of the Update call can not be assured.
	 **/
	void Update () {
		Debug.Log ("Update is called!");
		/**
		 * When called from the fixed update method, returns the fixed deltaTime.
		 **/
		Debug.Log (Time.deltaTime);
	
		transform.Rotate (Vector3.right * Time.deltaTime);
		if(Input.GetKeyDown(KeyCode.S)) {
			/**
			 * Active state is about game object.
			 * Enable state is for components.
			 **/
			togglable.SetActive (!togglable.activeSelf);
			Debug.Log (togglable.activeSelf);
		}

		if(Input.GetKeyDown(KeyCode.UpArrow)) {
			rotate_angle += 1.0f;
		} else if(Input.GetKeyDown(KeyCode.DownArrow)) {
			rotate_angle -= 1.0f;
		}

		if(Input.GetKeyDown(KeyCode.RightArrow)) {
			rotation_speed += 1.0f;
		} else if(Input.GetKeyDown(KeyCode.LeftArrow)) {
			rotation_speed -= 1.0f;
		}
		transform.Translate (Vector3.forward * rotate_angle * Time.deltaTime);
		transform.Rotate (Vector3.up, -rotation_speed * Time.deltaTime);
	}

	/**
	 * The FixedUpdate is called on a regular basis indepandantly of the rendering engine. Therefore, the regularity of this call can be "almost" assured, even if the rendering is slow.
	 **/
	void FixedUpdate() {
		Debug.Log ("FixedUpdate is called!");
	}
}
