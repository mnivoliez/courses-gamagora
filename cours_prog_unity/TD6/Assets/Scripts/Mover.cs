﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {

	public float MaxSpeed = 1f;
	private Transform ThisTransform = null;

	void Awake () {
		ThisTransform = GetComponent<Transform>();
	}

	void Update () {
		ThisTransform.position += ThisTransform.forward * MaxSpeed * Time.deltaTime;
	}
}
