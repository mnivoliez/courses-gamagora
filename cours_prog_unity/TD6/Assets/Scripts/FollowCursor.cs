﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCursor : MonoBehaviour {

	private Transform ThisTransform = null;

	void Awake () {
		ThisTransform = GetComponent<Transform> ();
	}

	void Update () {
		Vector3 posSourisMonde = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f));
		posSourisMonde = new Vector3 (posSourisMonde.x,	ThisTransform.position.y, posSourisMonde.z);
		Vector3 directionRegard = posSourisMonde - ThisTransform.position;
		ThisTransform.localRotation = Quaternion.LookRotation(directionRegard.normalized, Vector3.up);
	}


}
