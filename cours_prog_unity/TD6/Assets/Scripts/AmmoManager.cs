﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;

public class AmmoManager : MonoBehaviour {
	public GameObject ammoPrefab = null;
	public int poolSize = 100;
	private GameObject[] ammoArray;
	public static AmmoManager AmmoManagerSingleton = null;
	public Queue<Transform> ammoQueue;

	void Awake () {
		if (AmmoManagerSingleton != null) {
			DestroyImmediate (AmmoManagerSingleton);
			return;
		}
		ammoQueue = new Queue<Transform> ();
		AmmoManagerSingleton = this;
	}

	void Start() {
		ammoArray = new GameObject[poolSize];
		for (int i = 0; i < poolSize; i++) {
			ammoArray [i] = Instantiate (ammoPrefab, Vector3.zero, Quaternion.identity) as GameObject;
			Transform objTransform = ammoArray [i].GetComponent<Transform> ();
			AmmoManagerSingleton.ammoQueue.Enqueue (objTransform);
			objTransform.parent = GetComponent<Transform> ();
		}
	}

	public static Transform SpawnAmmo(Vector3 pos, Quaternion rotation){
		Transform spawnedAmmo = AmmoManagerSingleton.ammoQueue.Dequeue (); 
		spawnedAmmo.position = pos;
		spawnedAmmo.rotation = rotation;
		spawnedAmmo.gameObject.SetActive(true);
		AmmoManagerSingleton.ammoQueue.Enqueue(spawnedAmmo);
		return spawnedAmmo;
	}
}