﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour {

	public float lifetime = 0.1f;
	void OnEnable() {
		CancelInvoke ();
		Invoke ("Die", lifetime);
	}

	void Die() {
		CancelInvoke ();
		gameObject.SetActive (false);
	}
}
