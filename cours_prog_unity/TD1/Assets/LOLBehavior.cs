﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LOLBehavior : MonoBehaviour {

	[SerializeField]
	private int x;

	[SerializeField] string text;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		print_number(factoriel (x));
		string out_text = "";
		foreach (char e in text) {
			out_text += e + "\n";
		}
		Debug.Log (out_text);
	}

	void print_number(int x) {
		Debug.Log (x);
	}

	int factoriel(int x) {
		if (x < 1) {
			return 0;
		} else if (x == 1) {
			return 1;
		} else {
			return x * factoriel (x - 1); 
		}
	}
}
