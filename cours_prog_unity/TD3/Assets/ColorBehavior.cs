﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorBehavior : MonoBehaviour {

	[SerializeField] private Color color1;
	[SerializeField] private Color color2;
	[SerializeField] private Color color3;
	[SerializeField] private KeyCode key;
	private Renderer rend;

	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		/**
		 * Not needed with the getKey
		 **/
		/*if(isDown = Input.GetKeyDown(key)) {
			rend.material.shader = Shader.Find("Specular");
			rend.material.color = color1;
		}*/

		if(Input.GetKey(key)) {
			rend.material.shader = Shader.Find("Specular");
			rend.material.color = Color.Lerp (color1, color2, Mathf.PingPong(Time.time, 1));
		}

		if(Input.GetKeyUp(key)) {
			rend.material.shader = Shader.Find("Specular");
			rend.material.color = color3;	
		}
	}
}
