﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionBehavior : MonoBehaviour {

	[SerializeField] private Color color1;
	[SerializeField] private Color color2;
	private int col_count;
	private Renderer rend;

	// Use this for initialization
	void Start () {
		col_count = 0;
		rend = GetComponent<Renderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision col) {
		rend.material.shader = Shader.Find("Specular");
		rend.material.color = Color.Lerp (color1, color2, col_count);
		col_count++;
		if(col_count == 3) {
			Destroy (gameObject);
		}
	}
}
