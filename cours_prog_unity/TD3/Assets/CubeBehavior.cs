﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeBehavior : MonoBehaviour {

	[SerializeField] private Transform target;
	[SerializeField] private float range;
	[SerializeField] private float speed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt (target);
		float distance = Vector3.Distance (target.position, transform.position); 
		if(distance <= range && distance >= 2.0f) {
			Vector3 direction = Vector3.MoveTowards (transform.position, target.position, range);
			direction.Normalize ();
			transform.Translate ( direction * speed);
		}
	}
}
