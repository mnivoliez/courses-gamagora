﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBehavior : MonoBehaviour {
	private float xSpeed;
	private float zSpeed;

	// Use this for initialization
	void Start () {
		xSpeed = 0;
		zSpeed = 0;
	}
	
	// Update is called once per frame
	void Update () {
		float x = Input.GetAxisRaw ("Vertical");
		float z = Input.GetAxisRaw ("Horizontal");
		if(x == 0) {
			xSpeed = 0;
		} else {
			xSpeed += x * Time.deltaTime;
		}

		if(z == 0) {
			zSpeed = 0;
		} else {
			zSpeed += z * Time.deltaTime;
		}

		transform.Translate (new Vector3 (xSpeed, 0, zSpeed));
	}
}
