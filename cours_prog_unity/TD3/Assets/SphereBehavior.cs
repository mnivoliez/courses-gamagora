﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereBehavior : MonoBehaviour {
	

	[SerializeField] private float rotation_speed;
	[SerializeField] private float rotate_angle;

	void Start () {
	}
	
	void Update () {
		transform.Rotate (Vector3.right * Time.deltaTime);
		if(Input.GetKeyDown(KeyCode.S)) {
			/**
			 * If we destroy a component, the gameobject still lives.
			 **/
			Destroy (gameObject.GetComponent<MeshRenderer>());
			/**
			 * However, transform component can't be destroyed.
			 **/
			Destroy (gameObject.GetComponent<Transform>());

		}

		/**
		 * GetAxisRaw is a finite value wich can be -1, 0 or 1 while GetAxis return a non finite value between -1 and 1. 
		 * It also seems to be more progressive depending on how long we press the key.
		 * If used, the paramaters "Dead, Sensitivity and Gravity" will define respectivly
		 * - a limit under wich any axis absolute value return 0
		 * - a scale of which the value increase against the press time on key
		 * - the time before it takes to return to 0 value
		 **/
	
		Debug.Log ("GetAxis: " + Input.GetAxis ("Vertical") + "GetAxisRaw: " + Input.GetAxisRaw ("Vertical"));

		rotate_angle += Input.GetAxis ("Vertical");
		rotation_speed += Input.GetAxis ("Horizontal");

		transform.Translate (Vector3.forward * rotate_angle * Time.deltaTime);
		transform.Rotate (Vector3.up, -rotation_speed * Time.deltaTime);
	}

	void FixedUpdate() {
	}
}
