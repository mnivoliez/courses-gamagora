﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMovementBehavior : MonoBehaviour {

	[SerializeField] [Range(0, 360)] float rotation_by_deltatime;
	[SerializeField] [Range(0, 5)] float rotation_degrees;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.forward * rotation_degrees * Time.deltaTime);
		transform.Rotate (Vector3.up, rotation_by_deltatime * Time.deltaTime);	
	}
}
