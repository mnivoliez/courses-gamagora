﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructionBehavior : MonoBehaviour {

	/**
	 * The key here allow to param wich key kill wich object even if all objects have the same script.
	 **/
	[SerializeField] private KeyCode key;

	// Use this for initialization
	void Start () {
		
	}
	
	void Update () {
		if(Input.GetKeyDown(key)) {
			/**
			 * Now we destroy the oject.
			 **/
			Destroy (gameObject);

		}
	}
}
