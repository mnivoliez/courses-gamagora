﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrayContainer {
	private int[] _values { get; set; }

	public ArrayContainer(int lenght, int default_value) {
		_values= new int[lenght];
		for (int i = 0; i < lenght; i++) {
			_values[i] = default_value;
		}
	}

	public ArrayContainer(int [] values) {
		_values = values;
	}

	public override string ToString() {
		string out_str = "";
		foreach(int i in _values) {
			out_str += i + ", ";
		}
		return out_str;
	}

}

public class FibboBehavior : MonoBehaviour {
	[SerializeField] private int fibbo_limit;
	private ArrayContainer fibbo;

	// Use this for initialization
	void Start () {
		List<int> fibbo_list = new List<int> ();
		fibbo_list.Add (0);
		fibbo_list.Add (1);

		for (int i = 2; i < fibbo_limit; i++) {
			fibbo_list.Add (fibbo_list [i - 2] + fibbo_list [i - 1]);
		}
	
		fibbo = new ArrayContainer (fibbo_list.ToArray ());

		Debug.Log (fibbo.ToString());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
