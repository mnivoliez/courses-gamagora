﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerController : MonoBehaviour {
	[SerializeField] private float xSpeed;
	[SerializeField] private float ySpeed;
	public string zouif = "wouarf";

	// Use this for initialization
	void Start () {
		xSpeed = 0;
		ySpeed = 0;
	}
	
	// Update is called once per frame
	void Update () {
		float x = Input.GetAxisRaw ("Horizontal");
		float y = Input.GetAxisRaw ("Vertical");
		if(x == 0) {
			xSpeed = 0;
		} else {
			xSpeed += x * Time.deltaTime;
		}

		if(y == 0) {
			ySpeed = 0;
		} else {
			ySpeed += y * Time.deltaTime;
		}
			transform.Translate (new Vector3 (xSpeed, ySpeed, 0));	

		if(Input.GetKeyDown(KeyCode.C)) {
			zouif = "HEHE";
		}
	}

	void OnCollisionEnter(Collision col) {
	}
}
