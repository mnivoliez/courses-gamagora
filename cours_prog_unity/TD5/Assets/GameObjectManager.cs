﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectManager : MonoBehaviour {

	[SerializeField] private List<GameObject> _games_objects;
	[SerializeField] private PlayerController _player;
	[SerializeField] private float rate;
	private float _time;

	// Use this for initialization
	void Start () {
		/**
		 * The generic trait allow to define type as a parameter in a structure of method.
		 **/
		_games_objects = new List<GameObject> ();
		_games_objects.AddRange (GameObject.FindGameObjectsWithTag("Respawn"));
		_player = Instantiate<PlayerController> (_player);
		if(System.IO.File.Exists(Application.persistentDataPath+@"\Player.json")) {
			string json = System.IO.File.ReadAllText (Application.persistentDataPath + @"\Player.json");
			JsonUtility.FromJsonOverwrite (json, _player);
		} 
	}

	// Update is called once per frame
	void Update () {
		foreach (GameObject obj in _games_objects) {
			Color color = new Color (Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f));
			Renderer rend = obj.GetComponent<Renderer> ();
			rend.material.shader = Shader.Find ("Specular");
			rend.material.color = color;
		}

		if (_time >= rate) {
			if (Random.value < 0.9f && _games_objects.Count < 200) {
				GameObject obj = _games_objects [Random.Range (0, _games_objects.Count - 1)];
				_games_objects.Add (Instantiate (obj, new Vector3 (Random.Range (-10, 10), Random.Range (-10, 10), 0f), Quaternion.identity));
			} else {
				GameObject obj = _games_objects [Random.Range (0, _games_objects.Count - 1)];
				_games_objects.Remove (obj);
				Destroy (obj);
			} 
			_time = 0f;
		} else {
			_time += Time.deltaTime;
		}

		if(Input.GetKeyDown(KeyCode.P)) {
			string json = JsonUtility.ToJson(_player);
			Debug.Log (json);
			System.IO.File.WriteAllText (Application.persistentDataPath + @"\Player.json", json);
		}
	}
}
