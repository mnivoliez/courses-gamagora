﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionBehavior : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision col) {
		if (col.gameObject.CompareTag ("DUPLICATABLE")) {
			Instantiate (col.gameObject, new Vector3 (Random.Range (0, 10), Random.Range (0, 10), Random.Range (0, 10)), Quaternion.identity);
		}
	}
}
