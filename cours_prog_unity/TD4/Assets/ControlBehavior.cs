﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBehavior : MonoBehaviour {
	private float xSpeed;
	private float ySpeed;
	private bool inversed_control;

	// Use this for initialization
	void Start () {
		xSpeed = 0;
		ySpeed = 0;
		inversed_control = false;
	}
	
	// Update is called once per frame
	void Update () {
		float x = Input.GetAxisRaw ("Horizontal");
		float y = Input.GetAxisRaw ("Vertical");
		if(x == 0) {
			xSpeed = 0;
		} else {
			xSpeed += x * Time.deltaTime;
		}

		if(y == 0) {
			ySpeed = 0;
		} else {
			ySpeed += y * Time.deltaTime;
		}
		if (inversed_control) {
			transform.Translate (new Vector3 (xSpeed, ySpeed, 0) * -1);
		} else {
			transform.Translate (new Vector3 (xSpeed, ySpeed, 0));	
		}


	}

	void OnCollisionEnter(Collision col) {
		CompanionCube cc = col.gameObject.GetComponent<CompanionCube> ();
		if(cc != null) {
			if(cc.collision_property.Equals(CollisionTag.INVERT)) {
				inversed_control = !inversed_control;
			}
		}
	}
}
