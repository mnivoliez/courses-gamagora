﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionTag {
	public static string INVERT = "INVERT";
	public static string NONE = "NONE";
}

public class PNJManager : MonoBehaviour {

	[SerializeField] private CompanionCube prefab;
	[SerializeField] private KeyCode toggle_key;
	[SerializeField] private float gen_rate;
	[SerializeField] private bool gen_enabled;
	[SerializeField] private float destruct_timeout;
	[SerializeField] private Transform target;
	private float time_since_last_gen;

	// Use this for initialization
	void Start () {
		time_since_last_gen = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(toggle_key)) {
			gen_enabled = !gen_enabled;
		}
		if(gen_enabled && time_since_last_gen >= gen_rate) {
			CompanionCube companion = Instantiate (prefab, new Vector3 (Random.Range (-10, 10), Random.Range (-10, 10), 0f), Quaternion.identity);
			companion.color = new Color (Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f), Random.Range (0f, 1f));
			companion.selfDestruct = destruct_timeout;
			companion.ia = (CompanionCubeIA)Random.Range(0, System.Enum.GetValues(typeof(CompanionCubeIA)).Length);
			companion.target = target;
			companion.collision_property = Random.value < 0.5f ? CollisionTag.INVERT : CollisionTag.NONE;
			time_since_last_gen = 0;
		} else {
			time_since_last_gen += Time.deltaTime;
		}
	}
}
