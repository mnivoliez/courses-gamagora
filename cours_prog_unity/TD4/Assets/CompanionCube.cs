﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CompanionCubeIA {
	None,
	DO_NOT_LEAVE_ME,
	I_DONT_HATE_YOU,
	RANDOM
}

public class CompanionCube: MonoBehaviour {

	public Color color;
	public float selfDestruct;
	private float lifetime;
	public CompanionCubeIA ia;
	public Transform target;
	private float range;
	private float speed;
	public string collision_property;

	// Use this for initialization
	void Start () {
		lifetime = 0f;
		Renderer rend = GetComponent<Renderer>();
		rend.material.shader = Shader.Find("Specular");
		rend.material.color = color;
		switch(ia) {
		case CompanionCubeIA.DO_NOT_LEAVE_ME:
			range = 20f;
			speed = 5f;
			break;
		case CompanionCubeIA.I_DONT_HATE_YOU:
			range = 5f;
			speed = 0.5f;
			break;
		case CompanionCubeIA.RANDOM:
			range = 0f;
			speed = 1f;
			break;
		case CompanionCubeIA.None:
			range = 0f;
			speed = 0f;
			break;
		}
	}
	
	// Update is called once per frame
	void Update () {
		lifetime += Time.deltaTime;
		if(lifetime >= selfDestruct) {
			Destroy (gameObject);
		}
		switch(ia) {
		case CompanionCubeIA.DO_NOT_LEAVE_ME:
		case CompanionCubeIA.I_DONT_HATE_YOU:
			transform.LookAt (target);
			float distance = Vector3.Distance (target.position, transform.position); 
			if (distance <= range) {
				transform.position = Vector3.MoveTowards (transform.position, target.position, speed * Time.deltaTime);
			}
			break;
		case CompanionCubeIA.RANDOM:
			transform.Translate (Vector3.forward * Random.Range (0f, 5f) * Time.deltaTime);
			transform.Rotate (Vector3.up, Random.Range (0f, 360f) * Time.deltaTime);
			break;
		case CompanionCubeIA.None:
			break;
		}

	}
}
