#include "voxel.h"
#include "cmath"
#include <algorithm>
#include "cube.h"
#include <iostream>
#include <map>

using namespace voxel;

vox_grid voxel::init_grid(const int dim_size) {
  vox_grid grid(dim_size, 
    std::vector<std::vector<bool>>(dim_size, 
      std::vector<bool>(dim_size, false)));

  return grid;
};

bool voxel::is_cell_in_shape(const Shape & shape, const point3 origin, const float x, const float y, const float z, const float size) {
  auto middle_cell = point3(origin.x + x * size + size / 2, origin.y + y * size + size / 2, origin.z + z * size + size / 2);
  auto vec = shape.origin - middle_cell;
  switch (shape.kind) {
  case ShapeKind::sphere:
    return vec.distance() <= shape.radius;
  case ShapeKind::cuboid:
    auto in_width = vec.x >= shape.origin.x && vec.x <= shape.origin.x + shape.width;
    auto in_height = vec.y >= shape.origin.y && vec.y <= shape.origin.y + shape.height;
    auto in_depth = vec.z >= shape.origin.z && vec.z <= shape.origin.z + shape.depth;
    return in_width && in_height && in_depth;
  }
};

VoxelGrid voxel::generate_vox_grid(const std::vector<Shape> & shapes, const float vox_size) {
  point3 lowest_p;
  point3 hightest_p;
  float size = 0.f;
  for (auto shape : shapes) {
    point3 low_sh;
    point3 hight_sh;
    switch (shape.kind) {
    case ShapeKind::sphere:
      low_sh = shape.origin - shape.radius;
      hight_sh = shape.origin + shape.radius;
      break;
    case ShapeKind::cuboid:
      low_sh = shape.origin;
      hight_sh = { shape.origin.x + shape.width, shape.origin.y + shape.height, shape.origin.z + shape.depth };
      break;
    }

    if (lowest_p.x > low_sh.x) {
      lowest_p.x = low_sh.x;
    }
    if (lowest_p.y > low_sh.y) {
      lowest_p.y = low_sh.y;
    }
    if (lowest_p.z > low_sh.z) {
      lowest_p.z = low_sh.z;
    }

    if (hightest_p.x < hight_sh.x) {
      hightest_p.x = hight_sh.x;
    }
    if (hightest_p.y < hight_sh.y) {
      hightest_p.y = hight_sh.y;
    }
    if (hightest_p.z < hight_sh.z) {
      hightest_p.z = hight_sh.z;
    }
  }

  auto low_to_hight = hightest_p - lowest_p;
  if (low_to_hight.x > low_to_hight.y && low_to_hight.x > low_to_hight.z) {
    size = low_to_hight.x;
  } else if (low_to_hight.y > low_to_hight.z) {
    size = low_to_hight.y;
  } else {
    size = low_to_hight.z;
  }

  // correct size.
  auto dim_size = size / vox_size;

  auto grid = init_grid(dim_size);
  
  for (int x = 0; x < dim_size; ++x) {
    for (int y = 0; y < dim_size; ++y) {
      for (int z = 0; z < dim_size; ++z) {
        bool has_exclusion = false;
        int n_inclusion = 0;
        bool has_other = false;
        for (auto shape : shapes) {
          if (is_cell_in_shape(shape, lowest_p, x, y, z, vox_size)) {
            switch (shape.effect) {
            case ShapeEffect::none:
              has_other = true;
              break;
            case ShapeEffect::inclusive:
              ++n_inclusion;
              break;
            case ShapeEffect::exclusive:
              has_exclusion = true;
              break;
            }
          }
        }
        grid[x][y][z] = !has_exclusion && (has_other || n_inclusion >= 2);
      }
    }
  }
  return { lowest_p, grid, dim_size, vox_size};
};

void voxel::draw_voxel_grid(const VoxelGrid & grid) {
  for (int x = 0; x < grid.dim_size; ++x) {
    for (int y = 0; y < grid.dim_size; ++y) {
      for (int z = 0; z < grid.dim_size; ++z) {
        if (grid.grid[x][y][z]) {
          cube::draw(grid.origin.x + x *grid.cell_size, grid.origin.y + y*grid.cell_size, grid.origin.z + z*grid.cell_size, grid.cell_size);
        }
      }
    }
  }
};


/*
VoxNode::VoxNode(point3 origin, float size) {

}

VoxNode::~VoxNode() {}

void VoxNode::add_child(VoxNode node) {

}

void VoxNode::draw_shapes(const std::vector<Shape> &shapes) const {

}

bool intersect_shape(Shape &shape) const {

}

std::optional<std::vector<Shape>> VoxNode::intersect_shapes(const std::vector<Shape> shapes) const {

}

VoxNode build_vox_tree(const std::vector<Shape> &shapes) {
  float lowest_x = 0f;
  float lowest_y = 0f;
  float lowest_z = 0f;

  float biggest_x = 0f;
  float biggest_y = 0f;
  float biggest_z = 0f;
  
  for(auto shape : shapes) {
    
  }
} */

