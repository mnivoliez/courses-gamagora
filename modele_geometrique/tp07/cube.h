#pragma once

namespace cube {
  void draw(const float x, const float y, const float z, const float size);
}
