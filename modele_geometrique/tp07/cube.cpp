#include <iostream>
#include "GL/glut.h"
#include "struct.h"
#include "cube.h"


void cube::draw(const float x, const float y, const float z, const float size) {
	point3 center(x, y, z);

	glColor3f(0, 0, 1);
	// right
	glBegin(GL_TRIANGLES);
	glVertex3f(center.x + size, center.y + size, center.z + size);
	glVertex3f(center.x + size, center.y + size, center.z - size);
	glVertex3f(center.x + size, center.y - size, center.z - size);
	glEnd();

	glBegin(GL_TRIANGLES);
	glVertex3f(center.x + size, center.y - size, center.z - size);
	glVertex3f(center.x + size, center.y - size, center.z + size);
	glVertex3f(center.x + size, center.y + size, center.z + size);
	glEnd();

	// left
	glBegin(GL_TRIANGLES);
	glVertex3f(center.x - size, center.y + size, center.z + size);
	glVertex3f(center.x - size, center.y + size, center.z - size);
	glVertex3f(center.x - size, center.y - size, center.z - size);
	glEnd();

	glBegin(GL_TRIANGLES);
	glVertex3f(center.x - size, center.y - size, center.z - size);
	glVertex3f(center.x - size, center.y - size, center.z + size);
	glVertex3f(center.x - size, center.y + size, center.z + size);
	glEnd();

	glColor3f(0, 1, 0);
	// front
	glBegin(GL_TRIANGLES);
	glVertex3f(center.x + size, center.y + size, center.z - size);
	glVertex3f(center.x + size, center.y - size, center.z - size);
	glVertex3f(center.x - size, center.y - size, center.z - size);
	glEnd();

	glBegin(GL_TRIANGLES);
	glVertex3f(center.x - size, center.y - size, center.z - size);
	glVertex3f(center.x - size, center.y + size, center.z - size);
	glVertex3f(center.x + size, center.y + size, center.z - size);
	glEnd();

	// back
	glBegin(GL_TRIANGLES);
	glVertex3f(center.x + size, center.y + size, center.z + size);
	glVertex3f(center.x + size, center.y - size, center.z + size);
	glVertex3f(center.x - size, center.y - size, center.z + size);
	glEnd();

	glBegin(GL_TRIANGLES);
	glVertex3f(center.x - size, center.y - size, center.z + size);
	glVertex3f(center.x - size, center.y + size, center.z + size);
	glVertex3f(center.x + size, center.y + size, center.z + size);
	glEnd();

	glColor3f(1, 0, 0);
	// top
	glBegin(GL_TRIANGLES);
	glVertex3f(center.x + size, center.y + size, center.z + size);
	glVertex3f(center.x + size, center.y + size, center.z - size);
	glVertex3f(center.x - size, center.y + size, center.z - size);
	glEnd();

	glBegin(GL_TRIANGLES);
	glVertex3f(center.x - size, center.y + size, center.z - size);
	glVertex3f(center.x - size, center.y + size, center.z + size);
	glVertex3f(center.x + size, center.y + size, center.z + size);
	glEnd();

	// bottom
	glBegin(GL_TRIANGLES);
	glVertex3f(center.x + size, center.y - size, center.z + size);
	glVertex3f(center.x + size, center.y - size, center.z - size);
	glVertex3f(center.x - size, center.y - size, center.z - size);
	glEnd();

	glBegin(GL_TRIANGLES);
	glVertex3f(center.x - size, center.y - size, center.z - size);
	glVertex3f(center.x - size, center.y - size, center.z + size);
	glVertex3f(center.x + size, center.y - size, center.z + size);
	glEnd();

}
