#pragma once

#include <iostream>
using namespace std ;



# define M_PI		3.14159265358979323846



/******************* point3 ******************/

struct vector3 {
  float x; float y; float z;
  float distance() const;
};

class	point3{   

public:

	float      x;

	float      y;

	float      z;

	

	point3();                                        // (0,0,0) par defaut

	point3(const float X,const float Y,const float Z);

	

	bool         operator==(const point3 &op)const;

	point3&     operator=(const point3 &op);

	point3      operator+(const point3 &op)const;

	point3&     operator*=(const float op);          //produit par un scalaire

	point3      operator*(const float op)const;                 //idem

	point3&     operator/=(const float op);         //division par un scalaire

	point3      operator/(const float op)const;                 //idem

  point3 operator-(const float value) const;
  point3 operator+(const float value) const;
  vector3 operator-(const point3 &op) const;
	

	void		Permutation(point3 *A, point3 *B);	     // Permutation de deux points

	friend ostream&  operator<<(ostream& p, point3 op);

	friend istream&  operator>>(istream& p, point3 &op);

};

enum class ShapeKind { sphere, cuboid };
enum class ShapeEffect { none, inclusive, exclusive };
struct Shape {
  point3 origin;
  ShapeEffect effect;
  ShapeKind kind;
  union {
	struct { float radius; }; /* sphere */
	struct { float width; float height; float depth; }; /* cuboid */
  };
};
