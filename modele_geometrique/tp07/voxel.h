#pragma once

#include <vector>
#include "struct.h"
#include "cube.h"
#include <optional>

namespace voxel {
  typedef std::vector<std::vector<std::vector<bool>>> vox_grid;
  struct VoxelGrid {
    point3 origin;
    vox_grid grid;
    float dim_size;
    float cell_size;
  };
	
  vox_grid init_grid(const int dim_size);
  bool is_cell_in_shape(const Shape &shape, const point3 origin, const float x, const float y, const float z, const float size);
	VoxelGrid generate_vox_grid(const std::vector<Shape> &shapes, const float vox_size);
  
  void draw_voxel_grid(const VoxelGrid &grid);
/*
  class VoxNode {
    private: 
      point3 origin;
      float size;
      std::vector<VoxNode> children;
      bool intersect_shape(Shape &shape) const;
    public:
      VoxNode(const point3 origin, const float size);
      ~VoxNode();
      void add_child(VoxNode child);
      void draw_shapes(const std::vector<Shape> &shapes) const;
      std::optional<std::vector<Shape>> intersect_shapes(const std::vector<Shape> shapes) const;

    
  }

  VoxNode build_vox_tree(const std::vector<Shape> &shapes ); */
}
