/*
 *	Tableau des points permettant de g�rer les points de controles
 * On s�lectionne le point en se d�pla�ant avec + et -, ...
 * On s�lectionne ensuite si on veut faire monter, descendre amener vers la gauche ou la droite le point.
 *   d : translation � droite
 *   q : � gauche
 *   z : en haut
 *   s : en bas
 *   
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "GL/glut.h"
#include "struct.h"
#include <vector>
#include "cube.h"
#include "voxel.h"

/* au cas ou M_PI ne soit defini */
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#define ESC 27

struct {
  std::vector<Shape> shapes;
  int selected_shape;
  float angle_x = 0.0;
  float angle_y = 0.0;
} gs;

std::vector<point3> generate_circle(point3 origine, float rayon, int nbPoints, point3 normale) {
  vector<point3> pnts = vector<point3>();
  for (int i = 0; i <= nbPoints; i++) {
    float angle = 2 * M_PI*i / nbPoints;
    float pntX = normale.x * rayon * cos(angle) + 0 * rayon * sin(angle) + origine.x;
    float pntY = normale.y * rayon * cos(angle) + 0 * rayon * sin(angle) + origine.y;
    float pntZ = normale.z * rayon * cos(angle) + 1 * rayon * sin(angle) + origine.z;
    point3 pnt = point3(pntX, pntY, pntZ);
    pnts.push_back(pnt);
  }

  return pnts;
}

/* initialisation d'OpenGL*/
static void init()
{
  glClearColor(0.0, 0.0, 0.0, 0.0);
  Shape shape;
  shape.origin = point3(0, 0, 0);
  shape.effect = ShapeEffect::exclusive;
  shape.kind = ShapeKind::sphere;
  shape.radius = 6.0;
  gs.shapes.push_back(shape);

  Shape shape_1;
  shape_1.origin = point3(9, 0, 0);
  shape_1.effect = ShapeEffect::inclusive;
  shape_1.kind = ShapeKind::sphere;
  shape_1.radius = 4.0;
  gs.shapes.push_back(shape_1);

  Shape shape_2;
  shape_2.origin = point3(12, 0, 0);
  shape_2.effect = ShapeEffect::inclusive;
  shape_2.kind = ShapeKind::sphere;
  shape_2.radius = 4.0;
  gs.shapes.push_back(shape_2);
  
  Shape shape_3;
  shape_3.origin = point3(-12, 2, 2);
  shape_3.effect = ShapeEffect::none;
  shape_3.kind = ShapeKind::cuboid;
  shape_3.width = 20.0;
  shape_3.height = 4.0;
  shape_3.depth = 4.0;
  gs.shapes.push_back(shape_3);

  gs.selected_shape = 0;
}


/* Dessin */
void display(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);
  glLoadIdentity();

  glMatrixMode(GL_MODELVIEW);
  glRotatef(gs.angle_x, 1.0, 0.0, 0.0);
  glRotatef(gs.angle_y, 0.0, 1.0, 0.0);


  auto vox = voxel::generate_vox_grid(gs.shapes, 1);
  voxel::draw_voxel_grid(vox);
	
  glDisable(GL_DEPTH_TEST);

	
  glFlush();
}

/* Au cas ou la fenetre est modifiee ou deplacee */
void reshape(int w, int h)
{
   glViewport(0, 0, (GLsizei) w, (GLsizei) h);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   glOrtho(-20, 20, -20, 20, -10, 10);
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
}

void keyboard(unsigned char key, int x, int y)
{
   switch (key) {
     case ' ':
       gs.selected_shape = (gs.selected_shape + 1) % gs.shapes.size();
       break;
     case 'q': 
       gs.shapes[gs.selected_shape].origin.x -= 1;
       break;
    case 'd':
       gs.shapes[gs.selected_shape].origin.x += 1;
       break;
    case 'z':
       gs.shapes[gs.selected_shape].origin.y += 1;
       break;
    case 's':
       gs.shapes[gs.selected_shape].origin.y -= 1;
       break;
   case '6':
     gs.angle_y -= 1.5;
      break;
   case '4':
     gs.angle_y += 1.5;
      break;
   case '8':
     gs.angle_x -= 1.5;
      break;
   case '5':
     gs.angle_x += 1.5;
      break;
    default :
      break;
   }
   glutPostRedisplay();
}


GLvoid window_idle() {
  //gs.angle += 1.5;
  //glutPostRedisplay();

}

int main(int argc, char **argv)
{
   glutInitWindowSize(400, 400);
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
   glutCreateWindow("Voxel");
   init();
   glutReshapeFunc(reshape);
   glutKeyboardFunc(keyboard);
   glutDisplayFunc(display);
   glutIdleFunc(&window_idle);
   glutMainLoop();
   return 0;
}
