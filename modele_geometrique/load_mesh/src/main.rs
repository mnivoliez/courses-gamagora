mod obj;
mod load_mesh;

use load_mesh::from_file;

fn main() {
    let obj = from_file("buddha.off");
    println!("{:?}", obj);
}
