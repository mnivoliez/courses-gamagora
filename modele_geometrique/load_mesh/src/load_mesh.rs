use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::io::Error;

use obj::{Obj3D, Vertice};

#[derive(Debug, Copy, Clone)]
pub enum LoadFailure {
    FAIL_TO_OPEN_FILE,
    FORMAT_NOT_RECOGNIZED,
    FILE_CORRUPTED,
}

impl From<Error> for LoadFailure {
    fn from(_: Error) -> LoadFailure {
        LoadFailure::FAIL_TO_OPEN_FILE
    }
}

pub fn from_file(filename: &'static str) -> Result<Obj3D, LoadFailure> {
    let mut file = File::open(filename)?;
    let mut reader = BufReader::new(file);

    let mut line = String::new();
    reader.read_line(&mut line)?;

    match line.trim().as_ref() {
        "OFF" => from_off(reader),
        _ => Err(LoadFailure::FORMAT_NOT_RECOGNIZED),
    }
}

fn from_off(mut reader: BufReader<File>) -> Result<Obj3D, LoadFailure> {
    let mut line = String::new();
    reader.read_line(&mut line)?;
    let mut iter = line.split_whitespace();

    //get number of vertices
    let nb_vertices = iter.next().unwrap();
    let nb_vertices = nb_vertices.parse::<u32>().unwrap();

    let mut vertices = Vec::with_capacity(nb_vertices as usize);

    //get the number of faces
    let nb_faces = iter.next().unwrap();
    let nb_faces = nb_faces.parse::<u32>().unwrap();

    let mut faces: Vec<Vec<u32>> = Vec::with_capacity(nb_faces as usize);
    for _ in 0..nb_vertices {

        let mut line = String::new();
        reader.read_line(&mut line)?;

        let mut iter = line.split_whitespace();

        //get x of vertex
        let x = iter.next().unwrap();
        let x = x.parse::<f32>().unwrap();

        //get y of vertex
        let y = iter.next().unwrap();
        let y = y.parse::<f32>().unwrap();

        //get z of vertex
        let z = iter.next().unwrap();
        let z = z.parse::<f32>().unwrap();

        vertices.push(Vertice(x, y, z));
    }

    for _ in 0..nb_faces {
        let mut line = String::new();
        reader.read_line(&mut line)?;

        let mut iter = line.split_whitespace();

        let mut face = Vec::new();
        while let Some(v_id) = iter.next() {
            let v_id = v_id.parse::<u32>().unwrap();
            face.push(v_id);
        }
        faces.push(face);
    }
    Ok(Obj3D {
        vertices: vertices,
        faces: faces,
    })
}
