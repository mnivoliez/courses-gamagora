#[derive(Debug, Clone, Copy)]
pub struct Vertice(pub f32, pub f32, pub f32);

#[derive(Debug, Clone)]
pub struct Obj3D {
    pub vertices: Vec<Vertice>,
    pub faces: Vec<(u32, Vec<u32>)>,
}