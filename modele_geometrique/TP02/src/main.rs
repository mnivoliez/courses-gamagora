mod obj;
mod obj_formatter;

use obj::Point;
use obj::{generate_cone, generate_cylinder, generate_shere, generate_truncated_cone};
use obj_formatter::write_obj_to_file;

fn main() {
    let cyl = generate_cylinder(Point(0.0, 0.0, 0.0), 3.0, 8.0, 100);
    write_obj_to_file("cylinder.obj", &cyl);

    let sphere = generate_shere(Point(0.0, 0.0, 0.0), 1.0, 22, 11);
    write_obj_to_file("sphere.obj", &sphere);

    let cone = generate_cone(Point(0.0, 0.0, 0.0), 3.0, 8.0, 100);
    write_obj_to_file("cone.obj", &cone);

    let tr_cone = generate_truncated_cone(Point(0.0, 0.0, 0.0), 3.0, 8.0, 6.0, 100);
    write_obj_to_file("truncated_cone.obj", &tr_cone);
}
