use std::{f32, u32};
use std::f32::consts::PI;

pub struct Point(pub f32, pub f32, pub f32);

pub struct Obj {
    pub label: &'static str,
    pub faces: Vec<Vec<u32>>,
    pub points: Vec<Point>,
}

pub fn generate_cylinder(origin: Point, r: f32, h: f32, n: u32) -> Obj {
    let mut points = Vec::new();
    for i in 1..n + 1 {
        let (x, y, z) = (
            r * ((2.0 * PI * i as f32) / n as f32).cos() + origin.0,
            r * ((2.0 * PI * i as f32) / n as f32).sin() + origin.1,
            h / 2.0,
        );
        let p1 = Point(x, y, z + origin.2);
        let p2 = Point(x, y, -z + origin.2);
        points.push(p1);
        points.push(p2);
    }

    //add top and bottom points
    points.push(Point(0.0 + origin.0, 0.0 + origin.1, h / 2.0 + origin.2));
    points.push(Point(0.0 + origin.0, 0.0 + origin.1, -(h / 2.0) + origin.2));

    let mut faces = Vec::new();
    //body of cylinder
    for i in 0..n {
        let face = vec![
            1 + (2 * (i % n)),
            2 + (2 * (i % n)),
            2 + (2 * ((i + 1) % n)),
            1 + (2 * ((i + 1) % n)),
        ];
        faces.push(face);
    }
    for i in 0..n {
        let face = vec![1 + (2 * (i % n)), 1 + (2 * ((i + 1) % n)), (2 * n) + 1];
        faces.push(face);
        let face = vec![2 + (2 * (i % n)), 2 + (2 * ((i + 1) % n)), (2 * n) + 2];
        faces.push(face);
    }
    //top and bottom faces of cyl
    Obj {
        label: "cylinder",
        faces: faces,
        points: points,
    }
}

pub fn generate_shere(origin: Point, r: f32, meridians: u32, parallels: u32) -> Obj {
    assert!(r > 0.0, "A radius inferior or equal to zero? Seriously?");
    assert!(meridians >= 3, "You must have more than 3 meridians");
    assert!(parallels >= 2, "At least use 2 parallels");

    let mut points = Vec::new();

    points.push(Point(origin.0, origin.1 + r, origin.2));
    for p in 1..parallels {
        let polar = PI * p as f32 / parallels as f32;
        let sp = polar.sin();
        let cp = polar.cos();

        for m in 1..meridians + 1 {
            let azimuth = 2.0 * PI * m as f32 / meridians as f32; 
            let sa = azimuth.sin();
            let ca = azimuth.cos();

            let (x, y, z) = (
                r * sp * ca,
                r * cp,
                r * sp * sa
                            );
            points.push(Point(x,y,z));
 
        }

    }
    points.push(Point(origin.0, origin.1 -r, origin.2));

    let mut faces = Vec::new();
    // there is p paralle, so the there is p point between meridians
    for m in 0..meridians {
        faces.push(vec![
            1,
            (m + 1) % meridians + 2,
            m + 2
        ]);
    }

    for p in 0..parallels-2 {
        let aStart = p * meridians + 2;
        let bStart = (p + 1) * meridians + 2;
        for m in 0..meridians {
            let (a, a1, b, b1) = (
                aStart + m,
                aStart + (m + 1) % meridians,
                bStart + m,
                bStart + (m + 1) % meridians,

                                 );
            faces.push(vec![a, a1, b, b1])
        }
    }

    for m in 0..meridians {
        faces.push(vec![
            points.len,
            (m + 1) % meridians + meridians * (parallels - 2) + 2,
            m + meridians * (parallels - 2) + 2
        ]);
    }

    Obj {
        label: "sphere",
        faces: faces,
        points: points,
    }
}
