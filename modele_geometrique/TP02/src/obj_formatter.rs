use std::error::Error;
use std::io::prelude::*;
use std::fs::File;
use std::path::Path;

use obj::Obj;

pub fn write_obj_to_file(name: &'static str, obj: &Obj) {
    let path = Path::new(name);
    let display = path.display();

    let mut file = match File::create(&path) {
        Err(why) => panic!("couldn't create {}: {}", display, why.description()),
        Ok(file) => file,
    };

    let mut content = format!("# {0} \ng {0}\n", obj.label);

    for ref p1 in obj.points.iter() {
        content.push_str(&format!(
            "v {} {} {} \n",
            p1.0.to_string(),
            p1.1.to_string(),
            p1.2.to_string()
        ));
    }

    for face in obj.faces.iter() {
        let mut face_str = String::from("f ");
        for p in face.iter() {
            face_str.push_str(&format!("{} ", p));
        }
        face_str.push_str(&format!("\n"));
        content.push_str(face_str.as_str());
    }

    match file.write_all(content.as_bytes()) {
        Err(why) => panic!("couldn't write to {}: {}", display, why.description()),
        Ok(_) => println!("successfully wrote to {}", display),
    }
}
