﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour {

	[SerializeField] private int depth = 20;

	[SerializeField] private int width = 256;
	[SerializeField] private int height = 256;

	[SerializeField] private float scale = 20f;

	[SerializeField] private GameObject buildingPrefab;

	[SerializeField] private int building_footprint = 5;
	[SerializeField] private float building_frequencies = 5f;
	[SerializeField] private float building_threshold = 0.6f;

	// Use this for initialization
	void Start () {
		Terrain terrain = GetComponent<Terrain> ();
		terrain.terrainData = GenerateTerrain (terrain.terrainData);

	}

	void Update () {
		


	}

	TerrainData GenerateTerrain (TerrainData terrainData) {
		terrainData.heightmapResolution = width;

		terrainData.size = new Vector3 (width, depth, height);

		float[,] heightsmap = GenerateHeights ();

		terrainData.SetHeights (0, 0, heightsmap);

		GenerateBuilding (heightsmap);

		return terrainData;
	}

	private void GenerateBuilding (float[,] heightsmap) {
		for (int x = 0; x < width; x += building_footprint) {
			for (int y = 0; y < height; y += building_footprint) {
				if (BuildingExist (x, y)) {
					GameObject building = Instantiate (buildingPrefab);
					building.transform.position = new Vector3 (x - building_footprint, depth * heightsmap [x, y], y);
				}
			}
		}
	}

	float[,] GenerateHeights () {
		float[,] heights = new float[width, height];
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				heights [x, y] = CalculateHeight (x, y);
			}
		}

		return heights;
	}

	float CalculateHeight (int x, int y) {
		float xCoord = (float)x / width * scale;
		float yCoord = (float)y / height * scale;

		return Mathf.PerlinNoise (xCoord, yCoord);
	}

	bool BuildingExist (int x, int y) {
		float xCoord = (float)x / width * building_frequencies;
		float yCoord = (float)y / height * building_frequencies;

		return Mathf.PerlinNoise (xCoord, yCoord) > building_threshold; 
	}
}
