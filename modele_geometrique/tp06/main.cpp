/*
 *	Tableau des points permettant de g�rer les points de controles
 * On s�lectionne le point en se d�pla�ant avec + et -, ...
 * On s�lectionne ensuite si on veut faire monter, descendre amener vers la gauche ou la droite le point.
 *   d : translation � droite
 *   q : � gauche
 *   z : en haut
 *   s : en bas
 *   
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "GL/glut.h"
#include "struct.h"
#include <vector>

/* au cas ou M_PI ne soit defini */
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#define ESC 27

float tx=0;
float ty=0;

const int NUMBER_OF_POINTS_SURFACE = 4;

// Ordre de la courbre  : Ordre
// Degr� de la courbe = Ordre - 1
int Ordre = NUMBER_OF_POINTS_SURFACE;

// Point de controle selectionn�
int numPoint = 0;

point3 surface_control_point[2][NUMBER_OF_POINTS_SURFACE];

point3 operator*(const float s, const point3 &p) {
  return { p.x * s, p.y * s, p.z * s };
}

// Fonction Factorielle
int fact(int n)
{
  if(n <= 0) {
    return 1;
  } else if(n == 1) {
    return 1;
  } else {
    return n * fact(n-1);
  }
}



float Bernstein(int i, int n, float t)
{
  auto a = fact(n);
  auto b = fact(i) * fact(n - i);
  auto r0 =  a / b;
  auto r1 = std::pow(t, i); 
  auto r2 = std::pow( 1.0 - t, n-i );
  auto r = r0 * r1 * r2;
  return r;
}

float f1(float u) {
  return 2*std::pow(u, 3)-3*std::pow(u,2)+1;
}

float f2(float u) {
  return -2*std::pow(u,3)+3*std::pow(u,2);
}

float f3(float u) {
  return std::pow(u,3)-2*std::pow(u,2)+u;
}

float f4(float u) {
  return std::pow(u,3)-std::pow(u,2);
}

void draw_hermite(const point3 p1, const point3 p2, const point3 v1, const point3 v2) {
  glBegin(GL_LINE_STRIP);
  for(float u = 0.0; u<1.0; u+=0.10) {
    auto p = (f1(u)*p1)+(f2(u)*p2)+(f3(u)*v1)+(f4(u)*v2);
    glVertex3f(p.x, p.y, p.z);
  }
  glEnd();
}

std::vector<point3> compute_bezier_curves(const point3 control_points[], const float step) {
  vector<point3> curve;
  for(float t = 0; t <= 1.0; t += step) {
    point3 q(0,0,0); 
    for(int i = 0; i < NUMBER_OF_POINTS_SURFACE; ++i) {
      auto qi = control_points[i] * Bernstein(i, 3, t);
      q.x += qi.x; 
      q.y += qi.y; 
      q.z += qi.z; 
    }
    curve.push_back(q);
  } 
  return curve;
}

std::vector<point3> generate_circle(point3 origine, float rayon, int nbPoints, point3 normale) {
  vector<point3> pnts = vector<point3>();
  for (int i = 0; i <= nbPoints; i++) {
    float angle = 2 * M_PI*i / nbPoints;
    float pntX = normale.x * rayon * cos(angle) + 0 * rayon * sin(angle) + origine.x;
    float pntY = normale.y * rayon * cos(angle) + 0 * rayon * sin(angle) + origine.y;
    float pntZ = normale.z * rayon * cos(angle) + 1 * rayon * sin(angle) + origine.z;
    point3 pnt = point3(pntX, pntY, pntZ);
    pnts.push_back(pnt);
  }

  return pnts;
}
void draw_curve(std::vector<point3> &curve) {
  glBegin(GL_LINE_STRIP);
  for(auto p : curve) {
    glVertex3f(p.x, p.y, p.z);
  } 
  glEnd();
}

void draw_surface(std::vector<point3> &curve0, std::vector<point3> &curve1, bool closed) {
  /* for(int i = 0; i < curve0.size(); ++i) {
    glBegin(GL_LINE_STRIP);
      glVertex3f(curve0[i].x, curve0[i].y, curve0[i].z);
      glVertex3f(curve1[i].x, curve1[i].y, curve1[i].z);
      if(i < curve0.size() - 1 || closed) {
        glVertex3f(curve0[i+1].x, curve0[i+1].y, curve0[i+1].z);
      }
    glEnd();
  } */

  if (curve0.size() == curve1.size()) {
    for (int u = 0; u < curve0.size(); u++){
      point3 point1U = curve0[u];
      point3 point2U = curve1[u];

      float distXU = (point2U.x - point1U.x) / 4;
      float distYU = (point2U.y - point1U.y) / 4;
      float distZU = (point2U.z - point1U.z) / 4;

      glColor3f(1.0, 1.0, 1.0);
      // Ligne
      glBegin(GL_LINE_STRIP);
        glVertex3f(point1U.x, point1U.y, point1U.z);
        glVertex3f(point2U.x, point2U.y, point2U.z);
      glEnd();

      glColor3f(1.0, 1.0, 1.0);

      if (u < curve0.size() - 1) {
        point3 point1UP = curve0[u + 1];
        point3 point2UP = curve1[u + 1];

        float distXUP = (point2UP.x - point1UP.x);
        float distYUP = (point2UP.y - point1UP.y);
        float distZUP = (point2UP.z - point1UP.z);


        glBegin(GL_LINE_STRIP);
          glVertex3f(point1U.x, point1U.y, point1U.z);
          glVertex3f(point2UP.x, point2UP.y, point2UP.z);
        glEnd();
              
      }
    }
  }
}

void draw_tube(std::vector<point3> &curve) {
  glColor3f(1.0, 0.0, 1.0);

  draw_curve(curve);

  glColor3f(1.0, 1.0, 1.0);

  for(int i = 0; i < curve.size(); i++) {
    point3 normale = point3(0, 0, 1);
    if (i < curve.size() - 1) {
      point3 tangeante = point3(
          curve[i+1].x - curve[i].x,
          curve[i + 1].y - curve[i].y,
          curve[i + 1].y - curve[i].y);
      normale = point3(tangeante.y * 1,  - tangeante.x * 1, 0);
      float norme = sqrt(pow(normale.x, 2) + pow(normale.y, 2) + pow(normale.z, 2));
      normale = point3(normale.x/norme, normale.y/norme, normale.z/norme);
    } else {
      point3 tangeante = point3(curve[i].x - curve[i - 1].x,
          curve[i].y - curve[i - 1].y,
          curve[i].y - curve[i - 1].y);
      normale = point3(tangeante.y * 1, -tangeante.x * 1, 0);
      float norme = sqrt(pow(normale.x, 2) + pow(normale.y, 2) + pow(normale.z, 2));
      normale = point3(normale.x / norme, normale.y / norme, normale.z / norme);
    }

    vector<point3> pntsC1 = generate_circle(curve[i], 1, 10, normale);


    if (i < curve.size() - 1) {
      if (i < curve.size() - 2) {
        point3 tangeante2 = point3(
            curve[i + 2].x - curve[i + 1].x,
            curve[i + 2].y - curve[i + 1].y, 
            curve[i + 2].y - curve[i + 1].y);
        point3 normale2 = point3(tangeante2.y * 1, -tangeante2.x * 1, 0);
        float norme2 = sqrt(pow(normale2.x, 2) + pow(normale2.y, 2) + pow(normale2.z, 2));
        normale = point3(normale2.x / norme2, normale2.y / norme2, normale2.z / norme2);
      }

      vector<point3> pntsC2 = generate_circle(curve[i + 1], 1, 10, normale);

      draw_surface(pntsC1, pntsC2, true);
    }

    draw_curve(pntsC1);
  }
}



/* initialisation d'OpenGL*/
static void init()
{
  glClearColor(0.0, 0.0, 0.0, 0.0);

  // Initialisation des points de contr�les
  // On choisit de les intialiser selon une ligne
  for (int i = 0; i < NUMBER_OF_POINTS_SURFACE; i++) {
    surface_control_point[0][i] = point3(i - 5,i * 2,i);
  }
  for (int i = 0; i < NUMBER_OF_POINTS_SURFACE; i++) {
    surface_control_point[1][i] = point3(i + 5 ,i  * 2,i);
  }
 
}


/* Dessin */
void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
   
	glMatrixMode(GL_MODELVIEW);
	
	glLoadIdentity();
	
        surface_control_point[0][numPoint] = surface_control_point[0][numPoint] + point3(tx, ty, 0);
        surface_control_point[1][numPoint] = surface_control_point[1][numPoint] + point3(tx, ty, 0);

	// Enveloppe des points de controles
	glColor3f (1.0, 0.0, 0.0);
	glBegin(GL_LINE_STRIP);
        for (int i =0; i < NUMBER_OF_POINTS_SURFACE; i++)
        {
		 glVertex3f(
                     surface_control_point[0][i].x,
                     surface_control_point[0][i].y,
                     surface_control_point[0][i].z);
        }
        glEnd(); 
/*	glBegin(GL_LINE_STRIP);
        for (int i =0; i < NUMBER_OF_POINTS_SURFACE; i++)
        {
		 glVertex3f(
                     surface_control_point[1][i].x,
                     surface_control_point[1][i].y,
                     surface_control_point[1][i].z);
        }
        glEnd(); 
*/
	// Affichage du point de controle courant
	// On se d�place ensuite avec + et - 
    // � d'un point de contr�le au suivant (+)
    // � d'un point de contr�le au pr�c�dent (-)
	glColor3f (0.0, 0.0, 1.0);
        glBegin(GL_LINE_LOOP);
		glVertex3f(
                    surface_control_point[0][numPoint].x+0.1,
                    surface_control_point[0][numPoint].y+0.1,
                    surface_control_point[0][numPoint].z);
		glVertex3f(
                    surface_control_point[0][numPoint].x+0.1,
                    surface_control_point[0][numPoint].y-0.1,
                    surface_control_point[0][numPoint].z);
		glVertex3f(
                    surface_control_point[0][numPoint].x-0.1,
                    surface_control_point[0][numPoint].y-0.1,
                    surface_control_point[0][numPoint].z);
		glVertex3f(
                    surface_control_point[0][numPoint].x-0.1,
                    surface_control_point[0][numPoint].y+0.1,
                    surface_control_point[0][numPoint].z);
	glEnd(); 
/*	glBegin(GL_LINE_LOOP);
		glVertex3f(
                    surface_control_point[1][numPoint].x+0.1,
                    surface_control_point[1][numPoint].y+0.1,
                    surface_control_point[1][numPoint].z);
		glVertex3f(
                    surface_control_point[1][numPoint].x+0.1,
                    surface_control_point[1][numPoint].y-0.1,
                    surface_control_point[1][numPoint].z);
		glVertex3f(
                    surface_control_point[1][numPoint].x-0.1,
                    surface_control_point[1][numPoint].y-0.1,
                    surface_control_point[1][numPoint].z);
		glVertex3f(
                    surface_control_point[1][numPoint].x-0.1,
                    surface_control_point[1][numPoint].y+0.1,
                    surface_control_point[1][numPoint].z);
	glEnd(); 
        */

	// Dessiner ici la courbe de B�zier.
	// Vous devez avoir impl�ment� Bernstein pr�c�demment.
        
        //draw_bezier(TabPC[3], TabPC[4], TabPC[5], TabPC[6], 0.2);
        
        auto step = 1.0 / NUMBER_OF_POINTS_SURFACE;

        auto curve0 = compute_bezier_curves(surface_control_point[0], step);
        auto curve1 = compute_bezier_curves(surface_control_point[1], step);

        glColor3f(0.8,0.8,0.8);
    
        draw_curve(curve0);   
        //draw_curve(curve1);   

        //draw_surface(curve0, curve1, false);

        draw_tube(curve0);
	glFlush();
}

/* Au cas ou la fenetre est modifiee ou deplacee */
void reshape(int w, int h)
{
   glViewport(0, 0, (GLsizei) w, (GLsizei) h);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   glOrtho(-20, 20, -20, 20, -10, 10);
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
}

void keyboard(unsigned char key, int x, int y)
{
   switch (key) {
   case '+':
		if (numPoint < Ordre-1)
		   numPoint = numPoint + 1;
        else
            numPoint = 0;
		tx=0;
		ty=0;
		break;
   case '-':
		if (numPoint > 0) 
		   numPoint = numPoint - 1;
        else
            numPoint = Ordre-1;
		tx=0;
		ty=0;
		break;

   case 'd':
         tx=0.1;
         ty=0;
      break;
   case 'q':
         tx=-0.1;
         ty=0;
      break;
   case 'z':
         ty=0.1;
         tx=0;
      break;
   case 's':
         ty=-0.1;
         tx=0;
      break;
   case ESC:
      exit(0);
      break;
   default :
	   tx=0;
	   ty=0;
   }
   glutPostRedisplay();
}

int main(int argc, char **argv)
{
   glutInitWindowSize(400, 400);
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
   glutCreateWindow("Courbe de B�zier");
   init();
   glutReshapeFunc(reshape);
   glutKeyboardFunc(keyboard);
   glutDisplayFunc(display);
   glutMainLoop();
   return 0;
}
