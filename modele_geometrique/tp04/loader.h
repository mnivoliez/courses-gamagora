#ifndef LOADER_H
#define LOADER_H

#include "model.h"
#include <string>

namespace Loader {
Model *load(std::string filename);
void save(Model* m, const std::string filename);
}

#endif
