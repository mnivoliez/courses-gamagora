#include "loader.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <algorithm>

using namespace std;

int find_index_of(vector<shared_vertice> &v, shared_vertice& elem) {
  return std::find(v.begin(), v.end(), elem) - v.begin();
}

Model *Loader::load(const std::string filename) {
  auto model = new Model();
  ifstream file(filename, ios::in);
  if (file) {
    string line;
    // first get fileformat, expect OFF
    getline(file, line);
    if (line.find("OFF") != string::npos) {
      // get second line format as number_of_vertices numbers_of_faces
      // irrelevant
      getline(file, line);
      istringstream ss(line);

      int number_of_vertices, number_of_faces;
      ss >> number_of_vertices;
      ss >> number_of_faces;

      cout << "Get vertices" << endl;
      for (int i = 0; i < number_of_vertices; ++i) {
        getline(file, line);
        istringstream ss(line);
        float x, y, z;
        ss >> x;
        ss >> y;
        ss >> z;

        model->add_vertice(x, y, z);
      }
      cout << "Vertices extracted" << endl;

      cout << "Get faces" << endl;
      for (int i = 0; i < number_of_faces; ++i) {
        vector<int> vertices;
        getline(file, line);
        istringstream ss(line);
        string token;
        ss >> token;
        while (ss >> token) {
          vertices.push_back(stoi(token));
        }
        model->add_face(vertices);
      }
      cout << "Faces extracted" << endl;
    }
    file.close();
  } else {
    cerr << "Unable to open file." << endl;
  }
  return model;
}

void Loader::save(Model *m, const std::string filename) {
  ofstream file(filename, ofstream::out);
  if (file) {
    // write first line
    file << "OFF" << endl;
    // write number of vertice and faces
    file << m->get_vertices().size() << " " << m->get_faces().size() << " "
         << m->get_normales().size() << endl;
    //starting to write vertice
    for(auto v : m->get_vertices()) {
      file << v->x << " "<< v->y << " " << v->z << endl;
    }
    //starting to write faces
    for(auto f : m->get_faces()) {
      file << f.vertices.size() << " ";
      for (auto v : f.vertices) {
        file << find_index_of(m->get_vertices(), v) << " "; 
      } 
      file << endl;
    }
  }
}
