#ifndef MODEL_H
#define MODEL_H

#include <set>
#include <memory>
#include <vector>
#include <utility>

struct Vector {
  float x, y, z;
};

struct Vertice {
  float x, y, z;
};

typedef std::shared_ptr<Vertice> shared_vertice;

struct Face {
  std::vector<shared_vertice> vertices;
};

class Model {
  public:
    Model();
    ~Model();
    void add_vertice(float x, float y, float z);
    void add_face(std::vector<int> &vertices_index);
    std::vector<shared_vertice>& get_vertices();
    std::vector<Face>::iterator get_faces_it();
    std::vector<Face>& get_faces();
    std::vector<std::pair<Vertice, Vertice>>& get_normales();
    void remove_face(int index);
    void remove_faces(int start, int end);
    void normalize();
    void center();
    void calculate_sufaces_normal();
  private:
    std::vector<shared_vertice> _vertices;
    std::vector<Face> _faces;
    std::vector<std::pair<Vertice, Vertice>> _normales;
    void garbage_collect_vertices();
};

#endif
