#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "GL/glut.h"
#include <iostream>

#include "loader.h"
#include "model.h"

#define WIDTH 480
#define HEIGHT 480

#define RED 0
#define GREEN 0
#define BLUE 0
#define ALPHA 1
#define PI 3.14159

#define SCALE 20
#define SCALE_NORMAL 1.5 * SCALE

#define KEY_ESC 27

Model *model;
bool show_normals = false;

float angleX = 0.0f;
float angleY = 0.0f;

float tx = 0.0;
float ty = 0.0;
float tz = 0.0;

void init_scene();
void render_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height);
GLvoid window_key(unsigned char key, int x, int y);
GLvoid window_mouse(int x, int y);
GLvoid window_idle();

int main(int argc, char **argv) {
  // initialisation  des param�tres de GLUT en fonction
  // des arguments sur la ligne de commande
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

  // d�finition et cr�ation de la fen�tre graphique
  glutInitWindowSize(WIDTH, HEIGHT);
  glutInitWindowPosition(0, 0);
  glutCreateWindow("Load models");

  // load model
  model = Loader::load("bunny.off");
  model->normalize();
  model->center();
  model->remove_faces(0, 1000);
  model->calculate_sufaces_normal();

  // initialisation de OpenGL et de la sc�ne
  initGL();
  init_scene();

  // choix des proc�dures de callback pour
  // le trac� graphique
  glutDisplayFunc(&window_display);
  // le redimensionnement de la fen�tre
  glutReshapeFunc(&window_reshape);
  // la gestion des �v�nements clavier
  glutKeyboardFunc(&window_key);
  glutIdleFunc(&window_idle);
  // mouse management
  glutPassiveMotionFunc(window_mouse);
  // la boucle prinicipale de gestion des �v�nements utilisateur
  glutMainLoop();

  delete model;

  return 0;
}

// initialisation du fond de la fen�tre graphique : noir opaque

GLvoid initGL() {
  glShadeModel(GL_SMOOTH);
  glClearColor(RED, GREEN, BLUE, ALPHA);
  glEnable(GL_DEPTH_TEST);
}

void init_scene() {}

// fonction de call-back pour l�affichage dans la fen�tre

GLvoid window_display() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  render_scene();

  // trace la sc�ne grapnique qui vient juste d'�tre d�finie
  glFlush();
  glutSwapBuffers();
}

// fonction de call-back pour le redimensionnement de la fen�tre

GLvoid window_reshape(GLsizei width, GLsizei height) {
  glViewport(0, 0, width, height);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(-15.0, 15.0, -15.0, 15.0, -15.0, 15.0);

  // toutes les transformations suivantes s�appliquent au mod�le de vue
  glMatrixMode(GL_MODELVIEW);
}

// fonction de call-back pour la gestion des �v�nements clavier

GLvoid window_key(unsigned char key, int x, int y) {
  switch (key) {
  case 'n':
    show_normals = !show_normals;
    break;
  case 'p':
    Loader::save(model, "changed.off");
    break;
  case KEY_ESC:
    exit(1);
    break;

  default:
    printf("La touche %d n�est pas active.\n", key);
    break;
  }
  glutPostRedisplay();
}

GLvoid window_mouse(int x, int y) {
  angleX = x * 720 / WIDTH;              // gere l'axe Oy
  angleY = -(y * 180 / HEIGHT - 90) * 4; // gere l'axe Ox

  glutPostRedisplay();
}

GLvoid window_idle() {

  //  glutPostRedisplay();
}

void render_scene() {
  // c'est ici qu'on dessine
  glLoadIdentity();
  glRotatef(-angleY, 0.0f, 0.0f, 1.0f);
  glRotatef(angleX, 0.0f, 1.0f, 0.0f);
  glTranslatef(tx, ty, tz);

  glRotatef(-angleY, 0.0f, 0.0f, 1.0f);
  glRotatef(angleX, 0.0f, 1.0f, 0.0f);

  glPushMatrix();
  for (auto face : model->get_faces()) {
    glColor3f(static_cast<float>(rand()) / static_cast<float>(RAND_MAX), 1, 1);
    switch (face.vertices.size()) {
    case 4:
      glBegin(GL_POLYGON);
      for (auto vertex : face.vertices) {
        auto v = *vertex;
        glVertex3f(v.x * SCALE, v.y * SCALE, v.z * SCALE);
      }
      glEnd();
      break;
    case 3:
      glBegin(GL_TRIANGLES);
      for (auto vertex : face.vertices) {
        auto v = *vertex;
        glVertex3f(v.x * SCALE, v.y * SCALE, v.z * SCALE);
      }
      glEnd();

      break;
    default:
      break;
    }
  }
  if (show_normals) {
    glColor3f(0, 1, 0);
    for (auto n : model->get_normales()) {
      glBegin(GL_LINES);
      glVertex3f(n.first.x, n.first.y, n.first.z);
      glVertex3f(n.second.x * SCALE_NORMAL, n.second.y * SCALE_NORMAL,
                 n.second.z * SCALE_NORMAL);
      glEnd();
    }
  }

  glPopMatrix();
  glColor3f(0.0, 1.0, 0.0); // Y vert
  glBegin(GL_LINES);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 20, 0);
  glEnd();

  glColor3f(0.0, 0.0, 1.0); // Z bleu
  glBegin(GL_LINES);
  glVertex3f(0, 0, 0);
  glVertex3f(0, 0, 20);
  glEnd();

  glColor3f(1.0, 0.0, 0.0); // X rouge
  glBegin(GL_LINES);
  glVertex3f(0, 0, 0);
  glVertex3f(20, 0, 0);
  glEnd();
}
