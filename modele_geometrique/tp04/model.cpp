#include "model.h"

#include <iostream>

#include <algorithm>
#include <cmath>

Vector operator-(const Vertice &a, const Vertice &b) {
  return {a.x - b.x, a.y - b.y, a.z - b.z};
};

Vertice operator+(const Vertice &a, const Vector &b) {
  return {a.x + b.x, a.y + b.y, a.z + b.z};
}

Model::Model() {
}

Model::~Model() {
}

void Model::add_vertice(float x, float y, float z) {
  auto vertice = std::make_shared<Vertice>(Vertice{x,y,z});
  _vertices.push_back(vertice);
}

void Model::add_face(std::vector<int> &vertices_index) {
  std::vector<shared_vertice> vertices;
  for(auto i : vertices_index) {
    shared_vertice vertice_copy(_vertices[i]);
    vertices.push_back(vertice_copy);
  } 
  _faces.push_back(Face{ vertices });
}

std::vector<shared_vertice>& Model::get_vertices() {
  return _vertices;
}

std::vector<Face>::iterator Model::get_faces_it() { return _faces.begin(); }
std::vector<Face>& Model::get_faces() { return _faces; }
std::vector<std::pair<Vertice, Vertice>>& Model::get_normales() { 
  return _normales; 
}

void Model::normalize() {
  float max_pos = 0;
  for(auto vertex : _vertices) {
    auto v = *vertex;
    max_pos = std::max(max_pos, 
        std::max(std::abs(v.x), 
          std::max(std::abs(v.y), std::abs(v.z))));
  }
  for(auto vertex : _vertices) {
    vertex->x /= max_pos;
    vertex->y /= max_pos;
    vertex->z /= max_pos;
  }
}

void Model::center() {
  float sum_x = 0;
  float sum_y = 0;
  float sum_z = 0;
  for(auto vertex : _vertices) {
    auto v = *vertex;
    sum_x += v.x;
    sum_y += v.y;
    sum_z += v.z;
  }
  float offset_x = sum_x / _vertices.size();
  float offset_y = sum_y / _vertices.size();
  float offset_z = sum_z / _vertices.size();
  
  for(auto v : _vertices) {
    v->x -= offset_x;
    v->y -= offset_y;
    v->z -= offset_z;
  }
}

void Model::calculate_sufaces_normal(){
  for(auto face : _faces) {
    if(face.vertices.size() == 3) {
      auto A = *face.vertices[0];
      auto B = *face.vertices[1];
      auto C = *face.vertices[2];
      auto AB = B - A;
      auto AC = C - A;
      Vector normal;
      normal.x = AB.y * AC.z - AB.z * AC.y;
      normal.y = AB.z * AC.x - AB.x * AC.z;
      normal.z = AB.x * AC.z - AB.y * AC.z;

      Vertice center;
      center.x = (A.x + B.x + C.x) / 3;
      center.y = (A.y + B.y + C.y) / 3;
      center.z = (A.z + B.z + C.z) / 3;

      auto end_normal = center + normal;
      _normales.push_back(std::make_pair(center, end_normal));
    } else if(face.vertices.size() == 4) {
      auto A = *face.vertices[0];
      auto B = *face.vertices[1];
      auto C = *face.vertices[2];
      auto D = *face.vertices[3];

      auto AB = B - A;
      auto AC = C - A;
      Vector normalABC;
      normalABC.x = AB.y * AC.z - AB.z * AC.y;
      normalABC.y = AB.z * AC.x - AB.x * AC.z;
      normalABC.z = AB.x * AC.z - AB.y * AC.z;

      Vertice centerABC;
      centerABC.x = (A.x + B.x + C.x) / 3;
      centerABC.y = (A.y + B.y + C.y) / 3;
      centerABC.z = (A.z + B.z + C.z) / 3;

      auto end_normalABC = centerABC + normalABC;
      _normales.push_back(std::make_pair(centerABC, end_normalABC));

      auto DB = B - D;
      auto DC = C - D;
      Vector normalDBC;
      normalDBC.x = DB.y * DC.z - DB.z * DC.y;
      normalDBC.y = DB.z * DC.x - DB.x * DC.z;
      normalDBC.z = DB.x * DC.z - DB.y * DC.z;

      Vertice centerDBC;
      centerDBC.x = (D.x + B.x + C.x) / 3;
      centerDBC.y = (D.y + B.y + C.y) / 3;
      centerDBC.z = (D.z + B.z + C.z) / 3;

      auto end_normalDBC = centerDBC + normalDBC;
      _normales.push_back(std::make_pair(centerDBC, end_normalDBC));

    }
  }  
}

void Model::remove_face(int index) {
  _faces.erase(_faces.begin() + index);
  garbage_collect_vertices();  
}

void Model::remove_faces(int start, int end) {
  _faces.erase(_faces.begin() + start, _faces.begin() + end);
  garbage_collect_vertices();
}

void Model::garbage_collect_vertices() {
  for(auto it = _vertices.begin(); it != _vertices.end(); ) {
    if(it->use_count() == 1 ){
      it = _vertices.erase(it);
    } else {
      ++it;
    }
  }
}
