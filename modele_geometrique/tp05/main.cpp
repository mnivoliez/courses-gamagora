/*
 *	Tableau des points permettant de g�rer les points de controles
 * On s�lectionne le point en se d�pla�ant avec + et -, ...
 * On s�lectionne ensuite si on veut faire monter, descendre amener vers la gauche ou la droite le point.
 *   d : translation � droite
 *   q : � gauche
 *   z : en haut
 *   s : en bas
 *   
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "GL/glut.h"
#include "struct.h"

/* au cas ou M_PI ne soit defini */
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#define ESC 27

float tx=-5.0;
float ty=-5.0;

// Tableau des points de contr�les en global ...
point3 TabPC[10];
// Ordre de la courbre  : Ordre
// Degr� de la courbe = Ordre - 1
int Ordre = 10;

// Point de controle selectionn�
int numPoint = 0;

point3 operator*(const float s, const point3 &p) {
  return { p.x * s, p.y * s, p.z * s };
}

// Fonction Factorielle
int fact(int n)
{
  if(n <= 0) {
    return 1;
  } else if(n == 1) {
    return 1;
  } else {
    return n * fact(n-1);
  }
}



float Bernstein(int i, int n, float t)
{
  auto a = fact(n);
  auto b = fact(i) * fact(n - i);
  auto r0 =  a / b;
  auto r1 = std::pow(t, i); 
  auto r2 = std::pow( 1.0 - t, n-i );
  auto r = r0 * r1 * r2;
  return r;
}

float f1(float u) {
  return 2*std::pow(u, 3)-3*std::pow(u,2)+1;
}

float f2(float u) {
  return -2*std::pow(u,3)+3*std::pow(u,2);
}

float f3(float u) {
  return std::pow(u,3)-2*std::pow(u,2)+u;
}

float f4(float u) {
  return std::pow(u,3)-std::pow(u,2);
}

void draw_hermite(const point3 p1, const point3 p2, const point3 v1, const point3 v2) {
  glBegin(GL_LINE_STRIP);
  for(float u = 0.0; u<1.0; u+=0.10) {
    auto p = (f1(u)*p1)+(f2(u)*p2)+(f3(u)*v1)+(f4(u)*v2);
    glVertex3f(p.x, p.y, p.z);
  }
  glEnd();
}

void draw_bezier(const point3 p0, const point3 p1, const point3 p2, const point3 p3, const float step) {
  glBegin(GL_LINE_LOOP);
  for(float t = 0; t <= 1.0; t += 0.2) {
    point3 q(0,0,0); 
    auto qi0 = p0 * Bernstein(0, 3, t);
    std::cout << "P0:" << p0 << " qi0:" << qi0 << std::endl;
    auto qi1 = p1 * Bernstein(1, 3, t);
    auto qi2 = p2 * Bernstein(2, 3, t);
    auto qi3 = p3 * Bernstein(3, 3, t);
    q.x = qi0.x + qi1.x + qi2.x + qi3.x; 
    q.y = qi0.y + qi1.y + qi2.y + qi3.y; 
    q.z = qi0.z + qi1.z + qi2.z + qi3.z; 
    glVertex3f(q.x, q.y, q.z);
  }
  glEnd();
}

/* initialisation d'OpenGL*/
static void init()
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
	
	// Initialisation des points de contr�les
	// On choisit de les intialiser selon une ligne
	for (int i = 0; i < Ordre; i++)
	{
     TabPC[i] = point3(i,i,i);
    }
 
}


/* Dessin */
void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	
   
	glMatrixMode(GL_MODELVIEW);
	
	glLoadIdentity();

	
	//glTranslatef(tx,ty,0.0);

	

	point3 Ptemp;
	
	TabPC[numPoint]=TabPC[numPoint]+point3(tx,ty,0);
        if(numPoint == 2) {
          TabPC[4] = TabPC[4] + point3(-tx, -ty, 0);
        } else if (numPoint == 4) {
          TabPC[2] = TabPC[2] + point3(-tx, -ty, 0);
        } else if (numPoint == 3) {
          TabPC[2]=TabPC[2]+point3(tx,ty,0);
          TabPC[4]=TabPC[4]+point3(tx,ty,0);
        }

	// Enveloppe des points de controles
	glColor3f (1.0, 0.0, 0.0);
	glBegin(GL_LINE_STRIP);
        for (int i =0; i < Ordre; i++)
        {
		 glVertex3f(TabPC[i].x, TabPC[i].y, TabPC[i].z);
        }
        glEnd(); 

        draw_hermite(TabPC[0], TabPC[5], point3(-5,10,0), point3(10,-10,0));
	

	// Affichage du point de controle courant
	// On se d�place ensuite avec + et - 
    // � d'un point de contr�le au suivant (+)
    // � d'un point de contr�le au pr�c�dent (-)
	glColor3f (0.0, 0.0, 1.0);
	glBegin(GL_LINE_LOOP);
		glVertex3f(TabPC[numPoint].x+0.1, TabPC[numPoint].y+0.1, TabPC[numPoint].z);
		glVertex3f(TabPC[numPoint].x+0.1, TabPC[numPoint].y-0.1, TabPC[numPoint].z);
		glVertex3f(TabPC[numPoint].x-0.1, TabPC[numPoint].y-0.1, TabPC[numPoint].z);
		glVertex3f(TabPC[numPoint].x-0.1, TabPC[numPoint].y+0.1, TabPC[numPoint].z);
	glEnd(); 

	// Dessiner ici la courbe de B�zier.
	// Vous devez avoir impl�ment� Bernstein pr�c�demment.
        
	glColor3f (0.0, 1.0, 0.0);
        draw_bezier(TabPC[0], TabPC[1], TabPC[2], TabPC[3], 0.2);

	glColor3f (0.0, 1.0,1.0);
        draw_bezier(TabPC[3], TabPC[4], TabPC[5], TabPC[6], 0.2);
        

	glFlush();
}

/* Au cas ou la fenetre est modifiee ou deplacee */
void reshape(int w, int h)
{
   glViewport(0, 0, (GLsizei) w, (GLsizei) h);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   glOrtho(-20, 20, -20, 20, -10, 10);
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
}

void keyboard(unsigned char key, int x, int y)
{
   switch (key) {
   case '+':
		if (numPoint < Ordre-1)
		   numPoint = numPoint + 1;
        else
            numPoint = 0;
		tx=0;
		ty=0;
		break;
   case '-':
		if (numPoint > 0) 
		   numPoint = numPoint - 1;
        else
            numPoint = Ordre-1;
		tx=0;
		ty=0;
		break;

   case 'd':
         tx=0.1;
         ty=0;
      break;
   case 'q':
         tx=-0.1;
         ty=0;
      break;
   case 'z':
         ty=0.1;
         tx=0;
      break;
   case 's':
         ty=-0.1;
         tx=0;
      break;
   case ESC:
      exit(0);
      break;
   default :
	   tx=0;
	   ty=0;
   }
   glutPostRedisplay();
}

int main(int argc, char **argv)
{
   glutInitWindowSize(400, 400);
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
   glutCreateWindow("Courbe de B�zier");
   init();
   glutReshapeFunc(reshape);
   glutKeyboardFunc(keyboard);
   glutDisplayFunc(display);
   glutMainLoop();
   return 0;
}
