/*
 *	Tableau des points permettant de g�rer les points de controles
 * On s�lectionne le point en se d�pla�ant avec + et -, ...
 * On s�lectionne ensuite si on veut faire monter, descendre amener vers la gauche ou la droite le point.
 *   d : translation � droite
 *   q : � gauche
 *   z : en haut
 *   s : en bas
 *   
 */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "GL/glut.h"
#include "struct.h"
#include <vector>

/* au cas ou M_PI ne soit defini */
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#define ESC 27

struct {
  std::vector<point3> shape;
} gs;

/* initialisation d'OpenGL*/
static void init()
{
  glClearColor(0.0, 0.0, 0.0, 0.0);

  gs.shape.push_back(point3 { 0, 0, 0 }); 
  gs.shape.push_back(point3 { 0, 8, 0 }); 
  gs.shape.push_back(point3 { 3, 3, 0 }); 
  gs.shape.push_back(point3 { 6, 3, 0 }); 
  gs.shape.push_back(point3 { 8, 8, 0 }); 
  gs.shape.push_back(point3 { 8, 0, 0 }); 
}

point3 operator*(const float s, const point3 &p) {
  return { p.x * s, p.y * s, p.z * s };
}

point3 pk12i(const point3 pki, const point3 pki1) {
  return (3.0/4.0 * pki) + (1.0/4.0 * pki1);  
}

point3 pk12i1(const point3 pki, const point3 pki1) {
  return (1.0/4.0 * pki) + (3.0/4.0 * pki1);  
}

std::vector<point3> pass_chaikin(std::vector<point3> original_shape) {
  std::vector<point3> new_shape;
  for(int i = 0; i < original_shape.size(); ++i) {
    new_shape.push_back(pk12i(original_shape[i], original_shape[(i + 1) % original_shape.size()]));
    new_shape.push_back(pk12i1(original_shape[i], original_shape[(i + 1) % original_shape.size()]));
  }
  return new_shape;
}

/* Dessin */
void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
   
	glMatrixMode(GL_MODELVIEW);
	
	glLoadIdentity();

        glBegin(GL_LINE_LOOP);
        for(auto p : gs.shape) {
          glVertex3f(p.x, p.y, p.z);
        }
        glEnd();

	glFlush();
}

/* Au cas ou la fenetre est modifiee ou deplacee */
void reshape(int w, int h)
{
   glViewport(0, 0, (GLsizei) w, (GLsizei) h);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   glOrtho(-20, 20, -20, 20, -10, 10);
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
}

void keyboard(unsigned char key, int x, int y)
{
   switch (key) {
     case 'p':
       for(auto p : gs.shape) {
         std::cout << p << std::endl;
       }
       break;
     case 'n':
       gs.shape = pass_chaikin(gs.shape);
       break;
   default :
     break;
   }
   glutPostRedisplay();
}

int main(int argc, char **argv)
{
   glutInitWindowSize(400, 400);
   glutInit(&argc, argv);
   glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
   glutCreateWindow("Chaikin");
   init();
   glutReshapeFunc(reshape);
   glutKeyboardFunc(keyboard);
   glutDisplayFunc(display);
   glutMainLoop();
   return 0;
}
