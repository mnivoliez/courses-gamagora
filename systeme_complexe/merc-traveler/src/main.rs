extern crate ggez;
use ggez::conf;
use ggez::event;
use ggez::{Context, GameResult};
use ggez::graphics;
use ggez::graphics::{DrawMode, Point};
use std::time::Duration;

extern crate rand;
use rand::{thread_rng, Rng};

use std::cmp::Ordering;

extern crate nalgebra as na;

const GRID_X: f32 = 300.;
const GRID_Y: f32 = 300.;

const NUMBER_OF_NODES: usize = 150;
const NUMBER_OF_BEST_CANDIDATE: usize = 800;
const POPULATION: usize = 8000;
const MUTATION_NUMBER: usize = 10;
const MUTATION_PROBABILITY: f32 = 0.1;

/// A 2 dimensional point representing a location
type Point2 = na::Point2<f32>;

#[derive(Debug)]
struct Node {
    pos: Point2,
}

#[derive(Debug, Copy, Clone, PartialEq)]
struct NodeId {
    index: usize,
}

fn world_to_screen_coords(screen_width: u32, screen_height: u32, point: Point2) -> Point2 {
    let width = screen_width as f32;
    let height = screen_height as f32;
    let x = point.x + width / 2.0;
    let y = height - (point.y + height / 2.0);
    Point2::new(x, y)
}

#[derive(Debug)]
struct Graph {
    nodes: Vec<Node>,
}

impl Graph {
    fn new(nodes_number: usize) -> Graph {
        let nodes = generate_nodes(nodes_number);
        Graph { nodes: nodes }
    }

    fn get_distance(&self, id_a: &NodeId, id_b: &NodeId) -> f32 {
        let (a, b) = (&self.nodes[id_a.index], &self.nodes[id_b.index]);
        na::distance(&a.pos, &b.pos)
    }
}

fn generate_nodes(nb_nodes: usize) -> Vec<Node> {
    let mut rng = thread_rng();
    let mut nodes: Vec<Node> = Vec::new();
    for _ in 0..nb_nodes {
        nodes.push(Node {
            pos: Point2::new(
                rng.gen_range(-GRID_X, GRID_X),
                rng.gen_range(-GRID_Y, GRID_Y),
            ),
        });
    }
    nodes
}

#[derive(Debug, Clone)]
struct Path {
    nodes: Vec<NodeId>,
    cost: f32,
}

impl PartialOrd for Path {
    fn partial_cmp(&self, other: &Path) -> Option<Ordering> {
        self.cost.partial_cmp(&other.cost)
    }
}

impl PartialEq for Path {
    fn eq(&self, other: &Path) -> bool {
        self.cost == other.cost
    }
}

fn get_initial_generation(graph: &Graph) -> Vec<Path> {
    let mut rng = thread_rng();
    let mut paths = Vec::new();
    for _ in 0..POPULATION {
        let mut path = Path {
            nodes: Vec::new(),
            cost: 0.,
        };
        while path.nodes.len() < graph.nodes.len() {
            let node_id = NodeId {
                index: rng.gen_range(0, graph.nodes.len()),
            };
            if !path.nodes.contains(&node_id) {
                path.nodes.push(node_id);
                if path.nodes.len() > 1 {
                    let prev = path.nodes[path.nodes.len() - 1 - 1];
                    path.cost += graph.get_distance(&node_id, &prev);
                }
            }
        }
        // return to starting point
        let start_point = path.nodes[0].clone();
        path.nodes.push(start_point);
        let prev = path.nodes[path.nodes.len() - 1 - 1];
        path.cost += graph.get_distance(&start_point, &prev);
        paths.push(path);
    }
    paths.sort_by(|a, b| a.partial_cmp(b).unwrap_or(Ordering::Equal));
    paths
}

fn compute_next_gen(previous: &[Path], graph: &Graph) -> Vec<Path> {
    let mut paths = Vec::new();
    let mut rng = thread_rng();
    for i in  0..NUMBER_OF_BEST_CANDIDATE {
        paths.push(previous[i].clone());
    }
    for _ in 0..(POPULATION - NUMBER_OF_BEST_CANDIDATE) {
        let mut path = Path {
            nodes: Vec::new(),
            cost: 0.,
        };
        let father_path = &previous[rng.gen_range(0, NUMBER_OF_BEST_CANDIDATE)];
        let mother_path = &previous[rng.gen_range(0, previous.len())];
        let start_fath = rng.gen_range(0, father_path.nodes.len());
        for i in start_fath..father_path.nodes.len() {
            let node_id = father_path.nodes[i].clone();
            path.nodes.push(node_id);
            if path.nodes.len() > 1 {
                let prev = path.nodes[path.nodes.len() - 1 - 1];
                path.cost += graph.get_distance(&node_id, &prev);
            }
        }
        // mutation happen here
        for _ in 0..MUTATION_NUMBER {
            if rng.gen_range(0., 1.) < MUTATION_PROBABILITY {
                let node_id = NodeId {
                    index: rng.gen_range(0, graph.nodes.len()),
                };
                if !path.nodes.contains(&node_id) {
                    path.nodes.push(node_id);
                    if path.nodes.len() > 1 {
                        let prev = path.nodes[path.nodes.len() - 1 - 1];
                        path.cost += graph.get_distance(&node_id, &prev);
                    }
                }
            }
        }

        for node in &mother_path.nodes {
            let node_id = node.clone();
            if !path.nodes.contains(&node_id) {
                path.nodes.push(node_id);
                if path.nodes.len() > 1 {
                    let prev = path.nodes[path.nodes.len() - 1 - 1];
                    path.cost += graph.get_distance(&node_id, &prev);
                }
            }
        }
        // return to starting point
        let start_point = path.nodes[0].clone();
        path.nodes.push(start_point);
        let prev = path.nodes[path.nodes.len() - 1 - 1];
        path.cost += graph.get_distance(&start_point, &prev);

        paths.push(path);
    }
    paths.sort_by(|a, b| a.partial_cmp(b).unwrap_or(Ordering::Equal));
    paths
}

struct MainState {
    graph: Graph,
    generations: Vec<Path>,
    current_gen: usize,
    best_path: Path,
    best_gen: usize,
    screen_width: u32,
    screen_height: u32,
}

impl MainState {
    fn new(ctx: &mut Context) -> GameResult<MainState> {
        let graph = Graph::new(NUMBER_OF_NODES);
        let initial_generation = get_initial_generation(&graph);
        let best_path = initial_generation[0].clone();

        let s = MainState {
            graph: graph,
            generations: initial_generation,
            current_gen: 0,
            best_path: best_path,
            best_gen: 0,
            screen_width: ctx.conf.window_width,
            screen_height: ctx.conf.window_height,
        };
        Ok(s)
    }
}

impl event::EventHandler for MainState {
    fn update(&mut self, _ctx: &mut Context, _dt: Duration) -> GameResult<()> {
        let next_gen = compute_next_gen(&self.generations, &self.graph);
        self.current_gen += 1;
        {
            if next_gen[0].cost < self.best_path.cost {
                self.best_path = next_gen[0].clone();
                self.best_gen = self.current_gen;
            }
        }
        self.generations = next_gen;
        println!(
            "Current generation: {} \nBest path generation: {}\nBest path cost: {}",
            self.current_gen, self.best_gen, self.best_path.cost
        );
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx);
        
        let mut line_current_path = Vec::new();
        for node_id in &self.generations[0].nodes {
            let adjust_p = world_to_screen_coords(
                self.screen_width,
                self.screen_height,
                self.graph.nodes[node_id.index].pos,
            );
            line_current_path.push(Point::new(adjust_p.x, adjust_p.y));
        }
        graphics::set_color(ctx, graphics::Color::new(0., 0.5, 0.5, 1.))?;
        graphics::line(ctx, &line_current_path)?;

        let mut line_best_path = Vec::new();
        for node_id in &self.best_path.nodes {
            let adjust_p = world_to_screen_coords(
                self.screen_width,
                self.screen_height,
                self.graph.nodes[node_id.index].pos,
            );
            line_best_path.push(Point::new(adjust_p.x, adjust_p.y));
        }
        graphics::set_color(ctx, graphics::Color::new(0., 1., 0., 1.))?;
        graphics::line(ctx, &line_best_path)?;
    
        graphics::set_color(ctx, graphics::Color::new(1., 0., 0., 1.))?;
        for node in &self.graph.nodes {
            let adjust_p = world_to_screen_coords(self.screen_width, self.screen_height, node.pos);
            graphics::circle(
                ctx,
                DrawMode::Fill,
                Point::new(adjust_p.x, adjust_p.y),
                2.5,
                32,
            )?;
        }
        graphics::present(ctx);
        Ok(())
    }
}

pub fn main() {
    let c = conf::Conf::new();
    let ctx = &mut Context::load_from_conf("super_simple", "ggez", c).unwrap();
    let state = &mut MainState::new(ctx).unwrap();
    event::run(ctx, state).unwrap();
}
