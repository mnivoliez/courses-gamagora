# BOIDS

Here the boids execice.

Rust was used during, this assignement.

To use compile this work, use the following command:
```
curl https://sh.rustup.rs -sSf | sh
cd <path to boids project>
cargo run
```

Currently, the user can make boids follow him by pressing SPACE, he can make more boids by pressing P.
To move, please use the arrow keys:
* UP to thrust
* LEFT and RIGHT to change the rotation.

The boids will try to stay at a stable distance from each other and avoid walls too.
