extern crate nalgebra as na;

extern crate ggez;
extern crate rand;
use ggez::conf;
use ggez::event::*;
use ggez::{Context, GameResult};
use ggez::graphics;
use ggez::timer;
use std::time::Duration;

/// A 2 dimensional point representing a location
type Point2 = na::Point2<f32>;
/// A 2 dimensional vector representing an offset of a location
type Vector2 = na::Vector2<f32>;

fn vec_from_angle(angle: f32) -> Vector2 {
    let vx = angle.sin();
    let vy = angle.cos();
    Vector2::new(vx, vy)
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum ActorType {
    Player,
    Boid,
    Wall,
}

#[derive(Debug, Clone, Copy, PartialEq)]
struct Actor {
    tag: ActorType,
    pos: Point2,
    velocity: Vector2,
    bbox_size: f32,
    facing: f32,
    rvel: f32,
}

const BOIDS_RANGE_MAX: f32 = 100.;
const BOIDS_RANGE_MIN: f32 = 30.;
const BOID_MAX_VELOCITY: f32 = 20.;


const PLAYER_BBOX: f32 = 12.0;
const BOID_BBOX: f32 = 12.0;
const WALL_BBOX: f32 = 12.0;

fn create_player() -> Actor {
    Actor {
        tag: ActorType::Player,
        pos: Point2::origin(),
        velocity: na::zero(),
        bbox_size: PLAYER_BBOX,
        facing: 0.,
        rvel: 0.,
    }
}

fn create_boid() -> Actor {
    Actor {
        tag: ActorType::Boid,
        pos: Point2::new(rand::random::<f32>() * 100., rand::random::<f32>() * 100.),
        velocity: Vector2::new(rand::random::<f32>(), rand::random::<f32>()),
        bbox_size: BOID_BBOX,
        rvel: 0.,
        facing: 0.,
    }
}

fn create_wall(x: f32, y: f32) -> Actor {
    Actor {
        tag: ActorType::Wall,
        pos: Point2::new(x, y),
        velocity: na::zero(),
        bbox_size: WALL_BBOX,
        rvel: 0.,
        facing: 0.,
    }
}

const SPRITE_SIZE: u32 = 32;

// Acceleration in pixels per second, more or less.
const PLAYER_THRUST: f32 = 100.0;
// Rotation in radians per second.
const PLAYER_TURN_RATE: f32 = 3.05;

fn player_handle_input(actor: &mut Actor, input: &InputState, dt: f32) {
    actor.facing += dt * PLAYER_TURN_RATE * input.xaxis;

    if input.yaxis > 0.0 {
        player_thrust(actor, dt);
    }
}

fn player_thrust(actor: &mut Actor, dt: f32) {
    let direction_vector = vec_from_angle(actor.facing);
    let thrust_vector = direction_vector * (PLAYER_THRUST);
    actor.velocity += thrust_vector * (dt);
}

const MAX_PHYSICS_VEL: f32 = 250.0;

fn update_boids_position(
    boids: &mut Vec<Actor>,
    walls: &Vec<Actor>,
    follow_player: Option<&Actor>,
) {
    let mut velocities: Vec<Vector2> = Vec::new();
    for boid in &*boids {
        let mut close_boids: Vec<&Actor> = Vec::new();
        for other_boid in &*boids {
            if other_boid != boid {
                let distance = other_boid.pos - boid.pos;
                let distance = (distance.x.powi(2) + distance.y.powi(2)).sqrt();
                if distance < BOIDS_RANGE_MAX {
                    close_boids.push(&other_boid);
                }
            }
        }
        let mut avg_vel: Vector2 = boid.velocity;
        avg_vel -= boid_move_closer(&boid, &close_boids);
        avg_vel += boid_move_with(&close_boids);
        avg_vel -= boid_move_away(&boid, &close_boids);
        avg_vel -= boid_try_to_avoid_wall(&boid, &walls);
        avg_vel.x *= rand::random::<f32>() * 100.;
        avg_vel.y *= rand::random::<f32>() * 100.;
        avg_vel = boid_move(avg_vel);
        if let Some(&player) = follow_player {
            avg_vel += boid_move_to_player(&boid, &player);
        }
        velocities.push(avg_vel);
    }
    for boid in &mut *boids {
        boid.velocity = velocities[0];
        velocities.remove(0);
    }
}

fn boid_move_to_player(boid: &Actor, player: &Actor) -> Vector2 {
    (player.pos - boid.pos) / 5.
}

fn boid_move(mut velocity: Vector2) -> Vector2 {
    if velocity.x.abs() > BOID_MAX_VELOCITY || velocity.y.abs() > BOID_MAX_VELOCITY {
        let scale_factor = BOID_MAX_VELOCITY / velocity.x.abs().max(velocity.y.abs());
        velocity *= scale_factor;
    }
    velocity
}

fn boid_move_closer(boid: &Actor, boids: &Vec<&Actor>) -> Vector2 {
    if !boids.is_empty() {
        let mut avg_vec: Vector2 = na::zero();
        for other_boid in boids {
            avg_vec += boid.pos - other_boid.pos;
        }
        avg_vec /= boids.len() as f32;
        avg_vec / 100.
    } else {
        na::zero()
    }
}

fn boid_move_with(boids: &Vec<&Actor>) -> Vector2 {
    if !boids.is_empty() {
        let mut avg_vel: Vector2 = na::zero();
        for other_boid in boids {
            avg_vel += other_boid.velocity;
        }
        avg_vel /= boids.len() as f32;
        avg_vel / 40.
    } else {
        na::zero()
    }
}

fn boid_try_to_avoid_wall(boid: &Actor, walls: &Vec<Actor>) -> Vector2 {
    let sqrt_min_range = BOIDS_RANGE_MIN.sqrt();
    let mut dist_diff: Vector2 = na::zero();
    let mut nb_close: usize = 0;
    for wall in walls {
        let distance = wall.pos - boid.pos;
        let distance = (distance.x.powi(2) + distance.y.powi(2)).sqrt();
        if distance < BOIDS_RANGE_MIN + 10. {
            nb_close += 1;
            let mut v = boid.pos - wall.pos;
            if v.x >= 0. {
                v.x = sqrt_min_range - v.x;
            } else if v.x < 0. {
                v.x = -sqrt_min_range - v.x;
            }
            if v.y >= 0. {
                v.y = sqrt_min_range - v.y;
            } else if v.y < 0. {
                v.y = -sqrt_min_range - v.y;
            }
            dist_diff += v;
        }
    }
    if nb_close != 0 {
        dist_diff
    } else {
        na::zero()
    }
}

fn boid_move_away(boid: &Actor, boids: &Vec<&Actor>) -> Vector2 {
    if !boids.is_empty() {
        let sqrt_min_range = BOIDS_RANGE_MIN.sqrt();
        let mut dist_diff: Vector2 = na::zero();
        let mut nb_close: usize = 0;
        for other_boid in boids {
            let distance = other_boid.pos - boid.pos;
            let distance = (distance.x.powi(2) + distance.y.powi(2)).sqrt();
            if distance < BOIDS_RANGE_MIN {
                nb_close += 1;
                let mut v = boid.pos - other_boid.pos;
                if v.x >= 0. {
                    v.x = sqrt_min_range - v.x;
                } else if v.x < 0. {
                    v.x = -sqrt_min_range - v.x;
                }
                if v.y >= 0. {
                    v.y = sqrt_min_range - v.y;
                } else if v.y < 0. {
                    v.y = -sqrt_min_range - v.y;
                }
                dist_diff += v;
            }
        }
        if nb_close != 0 {
            dist_diff / 5.
        } else {
            na::zero()
        }
    } else {
        na::zero()
    }
}

fn update_collision_wall(actor: &mut Actor, walls: &Vec<Actor>) {
    for wall in &*walls {
        let pdistance = wall.pos - actor.pos;
        if pdistance.norm() < (actor.bbox_size + wall.bbox_size) {
            actor.velocity *= -1.;
        }
    }
}

fn update_actor_position(actor: &mut Actor, dt: f32) {
    // Clamp the velocity to the max efficiently
    let norm_sq = actor.velocity.norm_squared();
    if norm_sq > MAX_PHYSICS_VEL.powi(2) {
        actor.velocity = actor.velocity / norm_sq.sqrt() * MAX_PHYSICS_VEL;
    }
    let dv = actor.velocity * (dt);
    actor.pos += dv;
    actor.facing += actor.rvel;
}

/// Takes an actor and wraps its position to the bounds of the
/// screen, so if it goes off the left side of the screen it
/// will re-enter on the right side and so on.
fn wrap_actor_position(actor: &mut Actor, sx: f32, sy: f32) {
    // Wrap screen
    let screen_x_bounds = sx / 2.0;
    let screen_y_bounds = sy / 2.0;
    let sprite_half_size = (SPRITE_SIZE / 2) as f32;
    let actor_center = actor.pos - Vector2::new(-sprite_half_size, sprite_half_size);
    if actor_center.x > screen_x_bounds {
        actor.pos.x -= sx;
    } else if actor_center.x < -screen_x_bounds {
        actor.pos.x += sx;
    };
    if actor_center.y > screen_y_bounds {
        actor.pos.y -= sy;
    } else if actor_center.y < -screen_y_bounds {
        actor.pos.y += sy;
    }
}

fn world_to_screen_coords(screen_width: u32, screen_height: u32, point: Point2) -> Point2 {
    let width = screen_width as f32;
    let height = screen_height as f32;
    let x = point.x + width / 2.0;
    let y = height - (point.y + height / 2.0);
    Point2::new(x, y)
}

#[derive(Debug)]
struct InputState {
    xaxis: f32,
    yaxis: f32,
}

impl Default for InputState {
    fn default() -> Self {
        InputState {
            xaxis: 0.0,
            yaxis: 0.0,
        }
    }
}

struct Assets {
    player_image: graphics::Image,
    boid_image: graphics::Image,
    wall_image: graphics::Image,
}

impl Assets {
    fn new(ctx: &mut Context) -> GameResult<Assets> {
        let player_image = graphics::Image::new(ctx, "/player.png")?;
        let boid_image = graphics::Image::new(ctx, "/boid.png")?;
        let wall_image = graphics::Image::new(ctx, "/wall.png")?;

        Ok(Assets {
            player_image: player_image,
            boid_image: boid_image,
            wall_image: wall_image,
        })
    }

    fn actor_image(&mut self, actor: &Actor) -> &mut graphics::Image {
        match actor.tag {
            ActorType::Player => &mut self.player_image,
            ActorType::Boid => &mut self.boid_image,
            ActorType::Wall => &mut self.wall_image,
        }
    }
}

struct MainState {
    player: Actor,
    follow_player: bool,
    boids: Vec<Actor>,
    walls: Vec<Actor>,
    assets: Assets,
    screen_width: u32,
    screen_height: u32,
    input: InputState,
}

impl MainState {
    fn new(ctx: &mut Context) -> GameResult<MainState> {
        let s = MainState {
            player: create_player(),
            follow_player: false,
            boids: vec![create_boid(), create_boid(), create_boid(), create_boid()],
            walls: vec![create_wall(-40., 100.), create_wall(200., -75.)],
            assets: Assets::new(ctx)?,
            screen_width: ctx.conf.window_width,
            screen_height: ctx.conf.window_height,
            input: InputState::default(),
        };
        Ok(s)
    }
}

fn draw_actor(
    assets: &mut Assets,
    ctx: &mut Context,
    actor: &Actor,
    world_coords: (u32, u32),
) -> GameResult<()> {
    let (screen_w, screen_h) = world_coords;
    let pos = world_to_screen_coords(screen_w, screen_h, actor.pos);
    // let pos = Vec2::new(1.0, 1.0);
    let px = pos.x as f32;
    let py = pos.y as f32;
    let dest_point = graphics::Point::new(px, py);
    let image = assets.actor_image(actor);
    graphics::draw(ctx, image, dest_point, actor.facing as f32)
}

impl EventHandler for MainState {
    fn update(&mut self, ctx: &mut Context, _dt: Duration) -> GameResult<()> {
        const DESIRED_FPS: u64 = 60;
        if !timer::check_update_time(ctx, DESIRED_FPS) {
            return Ok(());
        }
        let seconds = 1.0 / (DESIRED_FPS as f32);

        // Update the player state based on the user input.
        player_handle_input(&mut self.player, &self.input, seconds);

        // Update the physics for all actors.
        // First the player...
        update_actor_position(&mut self.player, seconds);
        wrap_actor_position(
            &mut self.player,
            self.screen_width as f32,
            self.screen_height as f32,
        );

        //Then the boids
        {
            let mut follow_player: Option<&Actor> = None;
            if self.follow_player {
                follow_player = Some(&self.player);
            }
            update_boids_position(&mut self.boids, &self.walls, follow_player);
            for act in &mut self.boids {
                update_actor_position(act, seconds);
                wrap_actor_position(act, self.screen_width as f32, self.screen_height as f32);
            }
        }

        update_collision_wall(&mut self.player, &self.walls);
        for act in &mut self.boids {
            update_collision_wall(act, &self.walls);
        }

        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx);
        // Loop over all objects drawing them...
        {
            let assets = &mut self.assets;
            let coords = (self.screen_width, self.screen_height);

            let p = &self.player;
            draw_actor(assets, ctx, p, coords)?;

            for b in &self.boids {
                draw_actor(assets, ctx, b, coords)?;
            }

            for w in &self.walls {
                draw_actor(assets, ctx, w, coords)?;
            }
        }

        graphics::present(ctx);
        Ok(())
    }

    fn key_down_event(&mut self, keycode: Keycode, _keymod: Mod, _repeat: bool) {
        match keycode {
            Keycode::Up => {
                self.input.yaxis = 1.0;
            }
            Keycode::Left => {
                self.input.xaxis = -1.0;
            }
            Keycode::Right => {
                self.input.xaxis = 1.0;
            }
            Keycode::Space => {
                self.follow_player = !self.follow_player;
            }
            Keycode::P => {
                self.boids.push(create_boid());
            }
            _ => (), // Do nothing
        }
    }

    fn key_up_event(&mut self, keycode: Keycode, _keymod: Mod, _repeat: bool) {
        match keycode {
            Keycode::Up => {
                self.input.yaxis = 0.0;
            }
            Keycode::Left | Keycode::Right => {
                self.input.xaxis = 0.0;
            }
            _ => (), // Do nothing
        }
    }
}

pub fn main() {
    let mut c = conf::Conf::new();
    c.window_title = "BOIIIIIDS".to_string();
    c.window_width = 640;
    c.window_height = 480;
    c.window_icon = "/player.png".to_string();

    let ctx = &mut Context::load_from_conf("astroblasto", "ggez", c).unwrap();

    match MainState::new(ctx) {
        Err(e) => {
            println!("Could not load game!");
            println!("Error: {}", e);
        }
        Ok(ref mut game) => {
            let result = run(ctx, game);
            if let Err(e) = result {
                println!("Error encountered running game: {}", e);
            } else {
                println!("Game exited cleanly.");
            }
        }
    }
}
