use std::collections::{HashMap, HashSet};
use std::cmp;
use rand::random;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Position {
    pub x: usize,
    pub y: usize,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum TileType {
    PLANE,
    WATER,
}
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Tile {
    pub pos: Position,
    pub tile_type: TileType,
}

pub struct Grid {
    width: usize,
    height: usize,
    pub tiles: Vec<Tile>,
}

impl Grid {
    pub fn new(width: usize, height: usize) -> Grid {
        let mut tiles = Vec::new();
        for x in 0..width {
            for y in 0..height {
                if random::<bool>() && random::<bool>() {
                    tiles.push(Tile {
                        pos: Position { x: x, y: y },
                        tile_type: TileType::WATER,
                    });
                } else {
                    tiles.push(Tile {
                        pos: Position { x: x, y: y },
                        tile_type: TileType::PLANE,
                    });
                }
            }
        }
        Grid {
            width: width,
            height: height,
            tiles: tiles,
        }
    }

    pub fn get_width(&self) -> usize {
        self.width
    }

    pub fn get_height(&self) -> usize {
        self.height
    }

    pub fn get_tiles(&self) -> &Vec<Tile> {
        &self.tiles
    }

    pub fn pos_to_cell_id(&self, pos: &Position) -> usize {
        return pos.y + pos.x * self.height;
    }

    fn heuristic_cost_estimate(&self, start_id: usize, goal_id: usize) -> f32 {
        let start = self.tiles[start_id];
        let goal = self.tiles[goal_id];
        let x = goal.pos.x as f32 - start.pos.x as f32;
        let y = goal.pos.y as f32 - start.pos.y as f32;
        (x.powi(2) + y.powi(2)).sqrt()
    }

    fn reconstruct_path(&self, came_from: &HashMap<usize, usize>, last: usize) -> Vec<usize> {
        let mut current = last;
        let mut total_path = vec![current];
        while came_from.contains_key(&current) {
            current = *came_from.get(&current).unwrap();
            total_path.push(current);
        }

        total_path
    }

    fn get_neighbors_of(&self, tile_id: usize) -> Vec<usize> {
        let mut neighbors = Vec::new();
        let tile_pos = self.tiles[tile_id].pos;
        //get neighbors on x
        if tile_pos.x + 1 < self.width {
            if (self.tiles[tile_id + self.height].tile_type != TileType::WATER) {
                neighbors.push(tile_id + self.height);
            }
        }
        if tile_pos.x >= 1 {
            if (self.tiles[tile_id - self.height].tile_type != TileType::WATER) {
                neighbors.push(tile_id - self.height);
            }
        }
        if tile_pos.y + 1 < self.height {
            if (self.tiles[tile_id + 1].tile_type != TileType::WATER) {
                neighbors.push(tile_id + 1);
            }
        }
        if tile_pos.y >= 1 {
            if (self.tiles[tile_id - 1].tile_type != TileType::WATER) {
                neighbors.push(tile_id - 1);
            }
        }
        neighbors
    }

    pub fn a_star(&self, start: usize, goal: usize) -> Vec<usize> {
        let mut closed_set = HashSet::new();
        let mut open_set = HashSet::new();
        open_set.insert(start);

        let mut came_from = HashMap::new();

        let mut g_score = HashMap::new();
        g_score.insert(start, 0_f32);

        let mut f_score = HashMap::new();
        f_score.insert(start, self.heuristic_cost_estimate(start, goal));

        while !open_set.is_empty() {
            let mut current = *open_set.iter().next().unwrap();
            for key in &open_set {
                if f_score.contains_key(&key)
                    && f_score.get(&key).unwrap() < f_score.get(&current).unwrap()
                {
                    current = *key;
                }
            }

            if current == goal {
                return self.reconstruct_path(&came_from, current);
            }

            open_set.remove(&current);
            closed_set.insert(current);

            let neighbors = self.get_neighbors_of(current);
            for neighbor in neighbors {
                if closed_set.contains(&neighbor) {
                    continue;
                }

                if !open_set.contains(&neighbor) {
                    open_set.insert(neighbor);
                }

                let tentative_g_score =
                    *g_score.get(&current).unwrap() + self.distance_between(current, neighbor);
                if g_score.contains_key(&neighbor)
                    && tentative_g_score >= *g_score.get(&neighbor).unwrap()
                {
                    continue;
                }

                came_from.insert(neighbor, current);
                g_score.insert(neighbor, tentative_g_score);
                f_score.insert(
                    neighbor,
                    tentative_g_score + self.heuristic_cost_estimate(neighbor, goal),
                );
            }
        }
        Vec::new()
    }

    fn distance_between(&self, t1: usize, t2: usize) -> f32 {
        let (tile_1, tile_2) = (self.tiles[t1], self.tiles[t2]);
        ((tile_2.pos.x as f32 - tile_1.pos.x as f32).powi(2)
            + (tile_2.pos.y as f32 - tile_1.pos.y as f32).powi(2))
            .sqrt()
    }
}

#[cfg(test)]
mod Tests {

    use super::*;

    #[test]
    fn test_astar() {
        let grid = Grid::new(3, 3);
        let start = 0;
        let goal = 7;
        let path = grid.a_star(start, goal);
        assert!(vec![7, 4, 3, 0] == path || vec![7, 6, 3, 0] == path);
    }
}
