extern crate ggez;
extern crate rand;
use ggez::conf;
use ggez::event;
use ggez::event::{Keycode, Mod};
use ggez::{Context, GameResult};
use ggez::graphics;
use ggez::graphics::{DrawMode, Point, Rect};
use std::time::Duration;

mod grid;

use grid::{Grid, Position, Tile, TileType};

struct Player {
    pub pos: Position,
    pub life: i32,
}

struct Mob {
    pub pos: Position,
    pub life: i32,
}

struct MainState {
    pub grid: Grid,
    pub player: Player,
    pub mob: Mob,
    player_turn: bool,
    screen_width: u32,
    screen_height: u32,
}

impl MainState {
    fn new(ctx: &mut Context) -> GameResult<MainState> {
        let grid = Grid::new(12, 10);
        let s = MainState {
            player: Player {
                pos: Position { x: 0, y: 0 },
                life: 10,
            },
            mob: Mob {
                pos: Position { x: 5, y: 6 },
                life: 10,
            },
            player_turn: true,
            grid: grid,
            screen_width: ctx.conf.window_width,
            screen_height: ctx.conf.window_height,
        };
        Ok(s)
    }
}

impl event::EventHandler for MainState {
    fn update(&mut self, _ctx: &mut Context, _dt: Duration) -> GameResult<()> {
        if !self.player_turn {
            if self.player.pos == self.mob.pos {
                println!("Game over!");
                let _ = _ctx.quit();
            }
            //get path of mob
            let (pl_cell, en_cell) = (
                self.grid.pos_to_cell_id(&self.player.pos),
                self.grid.pos_to_cell_id(&self.mob.pos),
            );
            if pl_cell != en_cell {
                let path = self.grid.a_star(en_cell, pl_cell);
                if !path.is_empty() {
                    self.mob.pos = self.grid.get_tiles()[path[path.len() -2]].pos;
                }
            }
            self.player_turn = !self.player_turn;
        }

        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx);
        for tile in self.grid.get_tiles() {
            let tile_world = world_to_screen_coords(
                self.screen_width,
                self.screen_height,
                self.grid.get_width(),
                self.grid.get_height(),
                tile.pos,
            );
            let square_face = 0.8 * self.screen_width as f32 / self.grid.get_width() as f32;
            match tile.tile_type {
                TileType::PLANE => {
                    graphics::set_color(ctx, graphics::Color::new(0_f32, 1.0, 0_f32, 0.9));
                }
                TileType::WATER => {
                    graphics::set_color(ctx, graphics::Color::new(0_f32, 0_f32, 1.0, 0.9));
                }
            }
            graphics::rectangle(
                ctx,
                DrawMode::Fill,
                Rect::new(tile_world.x, tile_world.y, square_face, square_face),
            );
        }
        //draw player
        let pl_world = world_to_screen_coords(
            self.screen_width,
            self.screen_height,
            self.grid.get_width(),
            self.grid.get_height(),
            self.player.pos,
        );
        graphics::set_color(ctx, graphics::Color::new(1_f32, 1.0, 0_f32, 1.0));
        graphics::circle(
            ctx,
            DrawMode::Fill,
            graphics::Point {
                x: pl_world.x,
                y: pl_world.y,
            },
            0.4 * self.screen_width as f32 / self.grid.get_width() as f32,
            32,
        );
        //draw enemy
        let en_world = world_to_screen_coords(
            self.screen_width,
            self.screen_height,
            self.grid.get_width(),
            self.grid.get_height(),
            self.mob.pos,
        );
        graphics::set_color(ctx, graphics::Color::new(0_f32, 0.0, 0_f32, 1.0));
        graphics::circle(
            ctx,
            DrawMode::Fill,
            graphics::Point {
                x: en_world.x,
                y: en_world.y,
            },
            0.4 * self.screen_width as f32 / self.grid.get_width() as f32,
            32,
        );
        graphics::present(ctx);
        Ok(())
    }

    fn key_down_event(&mut self, keycode: Keycode, _keymod: Mod, _repeat: bool) {
        if self.player_turn {
            match keycode {
                Keycode::Up => if self.player.pos.y > 0 {
                    self.player.pos.y -= 1;
                },
                Keycode::Down => if self.player.pos.y < self.grid.get_height() - 1 {
                    self.player.pos.y += 1;
                },
                Keycode::Left => if self.player.pos.x > 0 {
                    self.player.pos.x -= 1;
                },
                Keycode::Right => if self.player.pos.x < self.grid.get_width() - 1 {
                    self.player.pos.x += 1;
                    println!("{:?}", self.player.pos);
                },
                _ => (),
            }
            self.player_turn = !self.player_turn;
        }
    }
}

fn world_to_screen_coords(
    screen_width: u32,
    screen_height: u32,
    grid_width: usize,
    grid_height: usize,
    point: Position,
) -> Point {
    let width = screen_width as f32 / grid_width as f32;
    let height = screen_height as f32 / grid_height as f32;
    let x = (point.x as f32 + 0.5) * width;
    let y = (point.y as f32 + 0.5) * height;
    Point::new(x, y)
}

pub fn main() {
    let c = conf::Conf::new();
    let ctx = &mut Context::load_from_conf("super_simple", "ggez", c).unwrap();
    let state = &mut MainState::new(ctx).unwrap();
    event::run(ctx, state).unwrap();
}
