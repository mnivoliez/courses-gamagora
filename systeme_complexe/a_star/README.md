# A``*``

J'ai implémenté A star suivant l'exemple fourni par Wikipedia.
Dans cette implémentation, nous nous basons sur un systeme de coordonnée en X et Y car noptre jeu est en 2D.
Les tiles sont soit des "plaines" soit de l'"eau".
L'eau est automatiquement exclu de l'algorithme.
Le jeu se joue au clavier avec les flèches directionnelles et se termine quand le joueur (en jaune) est touché par le monstre (en noir).
Le joueur peut se déplacer sur l'eau.

Mathieu Nivoliez
